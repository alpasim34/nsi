#!/usr/bin/env python 3
# -*- coding: utf-8 -*-


## importation des modules


## Déclaration des fonctions

def init_players():
    player_1 = input('Joueur 1, entrez votre nom : ')
    player_2 = input('Joueur 2, entrez votre nom : ')
    return player_1, player_2

def min_max(situation, player):
    if jeu.is_game_finished(situation, player):
        if player == 'current':
            return (jeu.eval_function(situation, player) * (+1), situation)
        else:
            return (jeu.eval_function(situation, player) * (-1), situation)
    else:
        if player == 'current':
            next_situations_list = jeu.next_situations(situation, player)
            min_max_list = [min_max(next_situation, 'other') for next_situation in next_situations_list]
#             print('Situation', situation)
#             print('  situations possibles pour other :', next_situations_list)
#             print('  current choisi pour other et donc prend le pire de', min_max_list)
            return min((min_max_list[i][0], next_situations_list[i]) for i in range(len(min_max_list)))
        else:
            next_situations_list = jeu.next_situations(situation, player)
            min_max_list = [min_max(next_situation, 'current') for next_situation in next_situations_list]
#             print('Situation', situation)
#             print('  situations possibles pour current :', next_situations_list)
#             print('  current choisi pour current et donc prend le meilleur de', min_max_list)
            return max((min_max_list[i][0], next_situations_list[i]) for i in range(len(min_max_list)))
    

## fonction principale

def main():
    other_player, current_player = init_players()
    current_situation = jeu.create_first_situation()
    
    while not jeu.is_game_finished(current_situation, current_player):
        current_player, other_player = other_player, current_player
        print()
        jeu.display_game(current_situation)
        if jeu.player_can_play(current_situation, current_player):
            next_situations_list = jeu.next_situations(current_situation, current_player)
            
            print(current_player, ', voici la liste des situations possibles :', sep='')
            for i in range(len(next_situations_list)):
                print('  situation', i, ':', next_situations_list[i], 'allumettes restantes')
        
            if current_player != 'IA':
                current_situation = jeu.human_player_plays(next_situations_list, current_player)
            else:
                choiced_situation = min_max(current_situation, 'current')
                print('IA choisi la situation :', choiced_situation[1], 'allumettes restantes.')
                current_situation = choiced_situation[1]
    
    winner = jeu.get_winner(current_situation, current_player, other_player)
    print()
    if winner == None:
        print('Aucun joueur ne gagne la partie.')
    else:
        print('Le joueur', winner, 'gagne la partie.')


## programme principal

if __name__ == '__main__':
    import jeu_de_nim as jeu
    main()