"""
    Ce module définit une abstraction pour les jeux à deux joueurs.
    Cette abstraction peut être utilisé dans un module `twoplayersgame`.

    Pour définir un jeu en particulier, vous devez choisir comment représenter une situation de jeu et un joueur
    et, ensuit, créer un mpodule qui implémentent les fonctions de ce module.

"""


def create_first_situation():
    """construit la situation initiale du jeu.
    create_first_situation() -> situation
    
    :returns: *(situation)* la situation initiale au début du jeu
    """
    raise NotImplementedError("initSituation doit être definie et renvoyer la situation initiale du jeu")


def is_game_finished(current_situation, current_player):
    """
    indique si *current_situation* est une situation de fin de jeu quand c'est à *current_player* de jouer
    gameIsFinished(situation, player) -> boolean

    :param current_situation: la situation couran
    :type situation: situation
    :param current_player: le joueur qui joue
    :type player: player
    :returns: *(boolean)* -- True si la situation courante termine le jeu
    """
    raise NotImplementedError("isFinished doit être definie comme une fonction qui teste la fin du jeu")


def player_can_play(current_situation, current_player):
    """
    indique si le joueur *current_player* peut jouer dans la situation *current_situation*
    player_can_play(current_situation, current_player) -> boolean

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(boolean)* -- True ssi le joueur peut jouer dans la situation donnée
    """
    raise NotImplementedError("playerCanPlay doit être definie pour déterminer si le joueur peut jouer")



def next_situations(current_situation, current_player):
    """
    renvoie la liste des situations qui peuvent être atteintes à partir de  *current_situation* quand c'est à  *current_player* de jouer
    nextSituations(situation) -> List<Situation>

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(list<situtation>)* -- la liste des situations qui peuvent être atteintes en un coup par le joueur
    """
    raise NotImplementedError("nextSituations  doit être definie comme une fonction qui fournit les situations atteignables en un coup (situations suivantes)")


def get_winner(current_situation, last_player, other_player):
    """
    renvoie le joueur vainqueur dans la situation  *current_situation* (supposée finale) quand *last_player*est le dernier joueur à avoir joué
    get_winner(current_situation, last_player, other_player) -> Player

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui a joué en dernier
    :type player: player
    :param other_player: l'autre joueur
    :type player: player
    :returns: *(Player)* -- le vainqueur
    """
    raise NotImplementedError("nextSituations doit être definie et calculer le vainqueur dans la situation donnée")


def display_game(current_situation):
    """
    affiche la situation *current_situation*
    display_game(current_situation) -> None
    
    :param current_situation: la situation courante
    :type situation: situation
    """
    raise NotImplementedError("displaySituation  doit être definie pour afficher une situation à l'écran")


def human_player_plays(current_situation, human_player):
    """
    fait jouer un coupe au joueur humain à partir de la situation *current_situation*, la situation atteinte est renvoyée
    human_player_plays(current_situation, human_player) -> Situation
    
    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(game situation)* la situation atteinte après que le joueur humain ait joué un coup
    """
    raise NotImplementedError( "humanPlayerPlays doit être definie pour faire jouer un coup au joueur humain, la situation atteinte est le résultat" )



def eval_function(current_situation, current_player):
    """
    La fonction d'évaluation évalue la situation *current_situation* pour le joueur *current_player*
    La fonction d'évaluation est croissante avec la qualité de la situation pour le joueur.
    evalFunction(situation, player) -> number value

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(number)* -- le score de la situation pour le joueur donné.
        Meilleure est la situation pour le joueur "min-Max", plus haut est le scole. La situation est inverse pour le joueur adverse (typiquement, le joueur humain).
    """
    raise NotImplementedError("evalFunction doit être definie comme la fonction d'évaluation d'une situation")
