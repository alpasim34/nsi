# Activité 1 : simulation de l'empreinte carbone de la salle de TP

Utiliser l'outil en ligne ci-dessous pour la calculer :

https://ecoinfo.cnrs.fr/ecodiag-calcul/

Vous rédigerez un compte-rendu qui contiendra au minimum :

- Les informations sur le site web utilisé (fondateurs, objectifs, type de société, modèle économique si évident, ...)
- Les services proposés
- L'utilité de la page utilisée
- Son utilisation (principe, paramétrage, ...)
- Les résultats
- Discussion des résultats
