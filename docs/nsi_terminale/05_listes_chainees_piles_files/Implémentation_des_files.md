# Implémentation des files

## A l'aide de tableau

Récupérer le fichier **file_list.py** sur le **git**

voici une trace des méthodes à implémenter

![trace](./figure/file.png)





1. La fonction **est_vide(file)** prend en paramètre une file sous forme de liste et test si elle est vide ou non. Elle renvoie vrai si la pile est vide et faux sinon. Ecrire le corps de cette fonction.

2. La fonction **enfiler(file,element)** prend en paramètre une file sous forme de liste et un élément et l'enfile,place donc l'élément à la fin de liste. Cette fonction fait appel à la méthode append du module list .Ecrire le corps de la fonction **enfiler**.

3. La fonction **defiler(file)** defile l'élément en tête de la file passée en paramètre et le renvoie. Cette fonction fait appel à la méthode pop avec l'argument 0. En effet l.pop(0) prive l de son premier élément et le renvoie.Ecrire le code de cette fonction.

4. La fonction **tete(file)** renvoie la tete de la file passée en paramètre.

    Écrire le corps de cette fonction.











## A l'aide de liste chainée

Récupérer le fichier **file_chainee.py** sur le **git**

Nous allons maintenant implémenter les files à l'aide d'une liste chainée dont le premier élément sera le dernier element à avoir été enfilé et donc la queue de la file.

Pour cela, nous allons utiliser la classe Maillon. On rappel que la classe Maillon contient 2 attributs :

- valeur pour la valeur du premier élément
- suivant pour le maillon suivant

voici une trace de la classe à implémenter

![trace](./figure/file_chainee.png)

### Constructeur

1. Lors de la création d'une file on initialise à None un attribut dernier_file. Le  **constructeur** ne prend pas de paramètre.  Écrire le corps de cette méthode.

### Méthode

2. La méthode **est_vide** effectue le test de vacuité de la file. Cette méthode test donc que  l'attribut dernier_file est  égal à None. Écrire le corps de cette méthode.

3. La méthode **enfiler** prend un paramètre élément et l'enfile dans la file, donc au début de la liste chainée stockée dans l'attribut dernier_file.  Compléter le corps de la méthode **enfiler**.

4. La méthode **defiler** defile l'élément en tete de la file et le renvoie. Cette méthode enlève donc le dernier élément de la liste chainée stockée dans dernier_file et le renvoie si celle-ci n'est pas vide et None sinon. Compléter le corps de cette méthode **defiler**.

5. La méthode **tete** renvoie la tete de la file.

   A quelle place dans la liste chainée stockée dans l'attribut dernier_file se situe la tete de la file?

    Compléter le corps de cette méthode.



## test

- Simuler l'exercice 2 à l'aide de la file à partir de tableau
- Simuler l'exercice 4 à l'aide de la file à partir de liste chainée



## A l'aide de tuple (à faire à la maison)

Refaire les fonctions suivante à l'aide de tuple et sans utilisation de méthodes python:

- est_vide(file)
- enfiler(file,element)
- defiler(file)
- tete(file)
