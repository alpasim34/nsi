# Implémentation des piles

## A l'aide de tableau

Récupérer le fichier **pile_list.py** sur le **git**

voici une trace des méthodes à implémenter

![trace](./figure/pile.png)





1. La fonction **est_vide(pile)** prend en paramètre une pile sous forme de liste et test si elle est vide ou non. Elle renvoie vrai si la pile est vide et faux sinon. Ecrire le corps de cette fonction.

2. La fonction **empiler(pile,element)** prend en paramètre une pile sous forme de liste et un élément et l'empile au sommet de la pile donc à la fin de liste. Cette fonction fait appel à la méthode append du module list .Ecrire le corps de la fonction **empiler**.

3. La fonction **depiler(pile)** depile l'élément au sommet de la pile passée en paramètre et le renvoie. Ecrire le corps de cette fonction.

4. La fonction **sommet(pile)** renvoie le sommet de la pile passée en paramètre.

    Écrire le corps de cette méthode.

## A l'aide de liste chainée

Récupérer le fichier **pile_chainee.py** sur le **git**

Nous allons maintenant implémenter les piles à l'aide d'une liste chainée dont le premier élément sera le sommet de la pile.

Pour cela, nous allons utiliser la classe Maillon. On rappel que la classe Maillon contient 2 attributs :

- valeur pour la valeur du premier élément
- suivant pour le maillon suivant

voici une trace de la classe à implémenter

![trace](./figure/pile_chainee.png)

### Constructeur

1. Lors de la création d'une pile on initialise à None un attribut haut. Le  **constructeur** ne prend pas de paramètre.  Écrire le corps de cette méthode.

### Méthode

2. La méthode **est_vide** effectue le test de vacuité de la pile. Cette méthode test donc que la taille de l'attribut haut est  égal à None. Écrire le corps de cette méthode.

3. La méthode **empiler** prend un paramètre élément et l'empile au sommet de la pile donc au début de la liste chainée stockée dans l'attribut haut.  Compléter le corps de la méthode **empiler**.

4. La méthode **depiler** depile l'élément au sommet de la pile et le renvoie. Cette méthode enlève donc le premier élément de la liste chainée stockée dans haut et le renvoie si celle-ci n'est pas vide et None sinon. Compléter le corps de cette méthode **depiler**.

5. La méthode **sommet** renvoie le sommet de la pile.

   A quelle place dans la liste chainée stockée dans l'attribut haut se situe le sommet de la pile?

    Écrire le corps de cette méthode.

## test

- Simuler l'exercice 1 à l'aide de la pile à partir de tableau
- Simuler l'exercice 3 à l'aide de la pile à partir de liste chainée



## A l'aide de tuple (à faire à la maison)

Refaire les fonctions suivante à l'aide de tuple et sans utilisation de méthodes python:

- est_vide(pile)
- empiler(pile,element)
- depiler(pile)
- sommet(pile)
