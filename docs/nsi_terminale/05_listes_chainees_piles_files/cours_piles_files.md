Il existe de nombreuses structures de données différentes. Choisir la plus adaptée permettra de traiter les données de manières plus simple, efficace et rapide.

- Une `structure de données linéaire`  regroupe des données de manière séquentielle : elles se suivent et peuvent être parcourue les unes après les autres dans un ordre précis. Nous étudierons les listes, les piles et les files.
- Dans une `structures de données non linéaires`, les données ne se suivent pas toutes de manière séquentielle mais leur organisation optimise certaines algorithmes que nous étudierons cette année. C'est le cas des arbres et des graphes.

------

# 1/Les piles

On retrouve dans les piles une partie des propriétés vues sur les  listes. Dans les piles, il est uniquement possible de manipuler le  dernier élément introduit dans la pile. On prend souvent l'analogie avec une pile d'assiettes : dans une pile d'assiettes la seule assiette directement accessible et la dernière assiette qui a été déposée sur  la pile. 	

Les piles sont basées sur le principe LIFO (Last In First Out : le dernier rentré sera le premier à sortir).

​			Voici les opérations primitives d'une pile : 	

- test de vacuité (pile_vide?) 		
- empiler un élément
- dépiler l'élément au sommet de la pile
- accéder à l'élément situé au sommet de la pile sans le supprimer de la pile (sommet) 		

# 2/ les files

Comme les piles, les files ont des points communs avec les listes.  Différences majeures : dans une file on ajoute des éléments à une  extrémité de la file et on supprime des éléments à l'autre extrémité. On prend souvent l'analogie de la file d'attente devant un magasin  pour décrire une file de données. 	

Les files sont basées sur le principe FIFO (First In First Out : le  premier qui est rentré sera le premier à sortir).

Voici les opérations primitives d'une file : 	

- test de vacuité(file_vide?) 		
- enfiler un élément
- défiler l'élément en tête de la file			
- accéder à l'élément situé en tête de la file sans le supprimer de la pile (tete) 	
