# Classe de terminale


## Consignes de rentrée

- **le cours :** il s'articule entre cours au tableau appuyé d'un diaporama, travaux pratiques avec ou sans ordinateur et travaux dirigés sans ordinateur.

- **le travail à la maison :** il consiste à rédiger au propre le cours en markdown, faire les exercices, devoirs à la maison, avancer les projets et réviser le tout régulièrement.

- **la préparation au baccalauréat :** il faut travailler régulièrement les annales des années précédentes et tous les exercices pratiques de la banque nationale des sujets afin de bien se préparer aux épreuves écrites et pratiques.

- **les évaluations :** elles consistent en des devoirs surveillés écrits et pratiques sanctionnés par une note sur 20. Les devoirs à la maison, les oraux, les projets et le cours en markdown sont aussi évalués sous la forme d'un bonus entre 0 et 2 points. La moyenne des bonus du trimestre augmente la moyenne. Ainsi, un travail personnel et sérieux permet facilement d'augmenter sa moyenne de 1 point même s'il est incomplet ou contient des erreurs. Un travail non rendu est sanctionné par un malus de 0.5 points. Un travail bâclé ou recopié est automatiquement évalué à 0 points.

- **matériel conseillé**:

    - une clé USB : pour sauvegarder programmes et documents. Ne pas oublier de sauvegarder son contenu chez soi en cas de perte ou de dysfonctionnement.
    - un classeur ou un porte-vues : pour ranger les documents papiers distribués en cours
    - un grand cahier : pour prendre des notes afin de pourvoir rédiger son cours au propre, noter les réponses aux diverses questions des TP et TD et les remarques.


**A FAIRE dès le premier cours :**

- Prise en main de la rédaction en markdown : [md](../outils/initiation_markdown.md)

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/) **(progression évaluée en cours)**

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/) **(progression évaluée en cours et à la maison, niveaux 1 et 2 validés fin de semaine prochaine + niveau 3 jusqu'au chapitre 7 inclus validés fin de semaine suivante)**


## Langages et programmation - Mise au point des programmes, gestion des bugs

Un langage de programmation est un langage formel. Son utilisation impose une rigueur mathématique pour l'écrire, l'interpréter et obtenir le bon comportement de la machine. Programmer n'est donc pas une tâche facile. Tout au long de l'apprentissage, le novice, tout au long de son travail, l'expert, rencontre erreurs et bugs. L'expérience personnelle et l'application des bonnes pratiques, issues de l'expérience des experts, permettra d'améliorer quotidiennement la qualité des programmes rédigés.

Pendant l'année, les problématiques classiques seront exposées, les situations rencontrées seront expliquées avec propositions de solutions.

- Prise en main de Python et bonnes pratiques : [md](../outils/initiation_python.md)

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/)

- Parcours Castor : [lien](https://concours.castor-informatique.fr/)

- Parcours Algorea : [lien](https://parcours.algorea.org/contents/4703/)

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/)


## Révisions - Algorithmes

Révision des algorithmes à connaître et évaluation de leur complexité. Permet de reposer les fondamentaux, notamment le parcours de tableau et les tris.

- Diapo cours : [pdf](./01_revisions_algo/algo_revisions_cours_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_cours_diapo.odp)
- Diapo TD : [pdf](./01_revisions_algo/algo_revisions_TD_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_TD_diapo.odp)
- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)


## Structures de données - Modularité

On aborde la modularité tout en révisant la programmation Python avec l'implémentation des algorithmes de base et l'évaluation de leurs performances.

- Diapo cours : [pdf](./02_modularite/Cours.pdf) ou [odp](./02_modularite/Cours.odp)
- Fiche TP : [pdf](./02_modularite/TP_modularite_algo.pdf) ou [odt](./02_modularite/TP_modularite_algo.odt)
- Modèles de fonction, programme et module Python : [zip](../outils/initiation_python/programmes_python_types/programmes_python_types.zip)


## Langages et programmation - Récursivité

Approche en algorithmique et programmation. Introduction aux fractales.

- Diapo cours TD : [pdf](./03_recursivite/cours_recusivite.pdf) ou [odp](./03_recursivite/cours_recusivite.odp)
- Fiche TP : [pdf](./03_recursivite/TP_recursivite.pdf) ou [odt](./03_recursivite/TP_recursivite.odt)
- Fiche arbre d'appel : [pdf](./03_recursivite/arbre_appel.pdf) ou [odp](./03_recursivite/arbre_appel.odp)
- Illustration de la recherche dichotomique récursive : [pdf](./13_techniques_programmation/SR_08_dichotomie_recursive.pdf) ou [odp](./13_techniques_programmation/SR_08_dichotomie_recursive.odp)


## Structures de données - Programmation objet

On approfondit la structuration des données et programmes. Encapsulation.

- Diapo cours : [pdf](./04_programmation_objet/cours_objets.pdf) ou [odp](./04_programmation_objet/cours_objets.odp)
- Fiche TP : [pdf](./04_programmation_objet/TP_programmation_objet.pdf) ou [odt](./04_programmation_objet/TP_programmation_objet.odt)
- Fiche TP "Un bouquet de fleurs" : [pdf](./04_programmation_objet/exo_fleurs.pdf) ou [odt](./04_programmation_objet/exo_fleurs.odt)
- Modèle de classe : [zip](../outils/initiation_python/programmes_python_types/modele_classe_objet.zip)
- Modèle de classe : [zip](../outils/initiation_python/programmes_python_types/modele_classe_objet.zip)

- Mini projet en classe sur l'animation : [pdf](../outils/interfaces_graphiques/tkinter_animation.pdf) ou [odt](../outils/interfaces_graphiques/tkinter_animation.odt)

- Mini projet guidé pour les vacances, jeu pierre-feuille-ciseaux : [pdf](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux_POO.pdf) ou [odt](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux_POO.odt)


## Bases de données - Bases de données relationnelles

- Diapo de cours sur l'introduction aux bases de données : [pdf](./09_bases_de_donnees/diapo_cours.pdf) ou [odp](./09_bases_de_donnees/diapo_cours.odp)
- TD sur le vocabulaire et les relations entre tables : [pdf](./09_bases_de_donnees/fiche_exercices_bdr.pdf) ou [odt](./09_bases_de_donnees/fiche_exercices_bdr.odt)
- Fiche sur l'utilisation de SQLite3 : [pdf](./09_bases_de_donnees/fiche_sqlite.pdf) ou [odt](./09_bases_de_donnees/fiche_sqlite.odt)
- Fiche TP 1 sur la création de tables : [pdf](./09_bases_de_donnees/TP_creations_tables.pdf) ou [odt](./09_bases_de_donnees/TP_creations_tables.odt)
- Fiche TP supplémentaire sur la création de tables : [pdf](./09_bases_de_donnees/tp_1_marette.pdf) ou [odt](./09_bases_de_donnees/tp_1_marette.odt)
    - Base de données pour le TP : [zip](./09_bases_de_donnees/lycee.zip)
- Fiche TP 2 sur le langage SQL : [pdf](./09_bases_de_donnees/TP_manipulation_tables.pdf) ou [odt](./09_bases_de_donnees/TP_manipulation_tables.odt)
    - Base de données pour le TP 2 : [zip](./09_bases_de_donnees/communes_nord.zip)
- Fiche sur le langage SQL : [pdf](./09_bases_de_donnees/fiche_SQL.pdf) ou [odt](./09_bases_de_donnees/fiche_SQL.odt)

<!-- - Résumé de cours : [md](./09_bases_de_donnees/resume_cours_1/resume.md) -->


<!--
- Fiche TD 2 sur le langage SQL : [pdf](./09_bases_de_donnees/TD_2.pdf) ou [odt](./09_bases_de_donnees/TD_2.odt)
- Base de donnée pour corriger le TD 2 : [zip](./09_bases_de_donnees/biblio.zip)

- Correction du TP 2 : [pdf](./09_bases_de_donnees/correction2TP.pdf) ou [odt](./09_bases_de_donnees/correction2TP.odt)
- Utilisation d'une base de données avec Python : [zip](./09_bases_de_donnees/biblio_python.zip)

- Fiche TD 3 sur les anomalies dans les bases de données relationnelles : [pdf](./09_bases_de_donnees/anomalies.pdf)
- Correction de l'exercice 6 : [pdf](./09_bases_de_donnees/correction_exercice_6.pdf) ou [odt](./09_bases_de_donnees/correction_exercice_6.odt) -->


## Corrections

[Dépôt des corrections](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_terminale/corrections_eleves)
