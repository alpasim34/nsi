# Algorithmes gloutons

## Pourquoi Glouton ?

Imaginons un livreur de colis qui doit passer dans 20 villes. Comment rendre son trajet le plus court possible ?

Et bien il n'y a qu'une seule façon de le savoir : lister tous les trajets possibles, les mesurer et choisir le plus court.

Sur le papier, le principe est simple. Mais ce n'est pas réalisable.

Faisons un peu de dénombrement.

- Considérons que le livreur se situe dans l'une des villes pour commencer le circuit.
- Pour sa première étape, il lui reste 19 possibilités : le nombre de trajets possibles à ce stade est donc 19
- Pour sa deuxième étape, il lui reste 18 possibilités : le nombre de trajets possibles à ce stade est donc 19×18 
- etc ...
- Pour son avant dernière étape, il ne lui reste que 2 possibilités : le nombre de trajets possibles à ce stade est donc 19×18×...×2=19! 
- Il n'a plus qu'à relier la ville restante pour terminer ses livraisons.
- Enfin, comme tout trajet peut se faire dans les deux sens avec la même distances, il y a $19!/2$ trajets de distances différentes. soit 6,08*10^16

Puisqu'il n'est pas possible de lister toutes les possibilités, il nous faut trouver une méthode conduisant à une réponse convenable. Celle-ci consistera à relier la ville la plus proche de la position du livreur à chaque étape, c'est à dire à choisir une solution **optimale localement** en espérant que la solution finale soit **proche de la solution optimale globale**.

Notre algorithme va donc prendre, à chaque étape, le meilleur cas possible. C'est pour cela qu'on dit qu'il est glouton, gourmand en quelque sorte. Notons que le terme anglais "greedy" se traduit également par cupide, avide, qui illustre bien l'idée de cet algorithme qui est de choisir la grande valeur possible à chaque étape.

