SYSTEME = [50, 20, 10, 5, 2, 1]

def plus_grosse_piece(somme_a_rendre):
    """
    renvoie la plus grosse piece du systeme inférieur ou égale à la somme à rendre
    :param somme_a_rendre:(int) la somme à rendre
    :return:(int) la plus grosse piece rendable possible
    """
    for piece in SYSTEME:
        if piece <= somme_a_rendre:
            return piece

def rendu_monnaie(somme_a_rendre):
    """
    renvoie une solution gloutonne du rendu de monnaie
    :param somme_a_rendre:(int) la somme à rendre
    :return:(dict) un dictionnaire  dont les clés sont les sommes des pièces/billets et les valeurs sont le nombre de fois qu'on les utilise pour rendre la monnaie
    """
    res={}
    while somme_a_rendre>0:
        piece=plus_grosse_piece(somme_a_rendre)
        if piece in res:
            res[piece]+=1
        else:
            res[piece]=1
        somme_a_rendre-=piece
    return res