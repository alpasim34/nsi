## Adaptation du périmètre d'évaluation de l'épreuve de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale à compter de la session 2022

[Texte officiel pour toutes les matières](https://www.education.gouv.fr/pid285/bulletin_officiel.html?pid_bo=40442)

À compter de la session 2022 du baccalauréat, les parties du programme de terminale qui ne pourront pas faire l'objet d'une évaluation lors de l'épreuve terminale écrite et pratique de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale de la voie générale définie dans la note de service n° 2020-030 du 11 février 2020 sont définies comme suit :

**1. Histoire de l'informatique :**

- Événements clés de l'histoire de l'informatique

**2. Structures de données :**

- Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés

**3. Bases de données :**

- Système de gestion de bases de données relationnelles

**4. Architectures matérielles, systèmes d'exploitation et réseaux** :

- Sécurisation des communications

**5. Langages et programmation :**

- Notions de programme en tant que donnée. Calculabilité, décidabilité

- Paradigmes de programmation

**6. Algorithmique :**

- Algorithmes sur les graphes

- Programmation dynamique

- Recherche textuelle
