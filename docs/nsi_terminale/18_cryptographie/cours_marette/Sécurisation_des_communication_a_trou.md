# La Sécurisation des communications 



## I/ Cryptographie

La cryptographie est la science du ..................., opération qui permet à deux individus d'échanger des messages de manière confidentielle à travers un canal ...........................



Pour réaliser cet objectif, les deux individus conviennent d'un ..........................., permettant de transformer le message appelé ........................... en un message **inintelligible** appelé........................... afin que l'attaquant éventuel ne puisse en prendre connaissance.

Cette transformation doit être ........................... : si on connait le secret et le *chiffré*, alors on peut reconstituer le *clair*.



- Un bon système de chiffrement ne doit et ne peut pas reposer uniquement sur le caractère secret de la ........................... employée .

  Si tel était le cas, il faudrait changer de méthode dès qu'elle serait ..........................., ce qui peut être coûteux, voir impossible. Au contraire, il faut considérer que l'attaquant connait le ............................ 

- La sécurité d'un système de chiffrement doit reposer sur :

  1. La difficulté à calculer le ........................... à partir du chiffré sans la connaissance du ........................... ,
  2. La difficulté à retrouver le ........................... ;

- Secret = mot(s), ou  nombre(s) appelés ............................

  La méthode de chiffrement explique :

  - comment à partir des clés et du clair, on produit le ...........................;
  - comment, à partir du chiffré et des clés, on produit le ............................



## II/Chiffrement symétrique 

### Définition
Dans les méthodes de chiffrement symétrique, la clé est ........................... : elle sert à la fois au ........................... et au ............................

Si Alice (A) veut envoyer un message ........................... à Bob (B), elle chiffre le message en utilisant une clé ...........................entre eux deux Pour déchiffrer le message, Bob utilise la ............................

![chiffrement symetrique](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/sym_A_B.png)

## Avantages

Le chiffrement symétrique est une méthode........................... et ........................... 

### Inconvénients

1. La clé doit d'abord être échangée entre les protagonistes, au moyen d'un ............................

    [![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/sym_echange_cle.png)]()

2. Si Alice veut échanger de manière sécurisée avec $n-1$ participants, alors ........................... clés sont nécessaires.

3. Par conséquent, si ........................... personnes veulent communiquer de manière sécurisée, alors $\frac{n(n-1)}{2}$ clés sont nécessaires.

## Exemple de chiffrements symétriques 

Le code de César et le chiffrement de Vigenère sont des chiffremnts symétriques

## III/Chiffrement asymétrique 

### Principe

Dans les méthodes de chiffrement asymétrique, chaque participant possède une **paire de clés** :

- une clé ..........................., que (comme son nom l'indique) tout le monde peut obtenir;
- une clé ...........................,  propriété exclusive de son propriétaire.

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_A.png)]()

### Propriétés

Ces deux clés sont **liées** l'une à l'autre.

Le système repose sur la propriété suivante :

- un message chiffré avec la clé publique ne pourra être déchiffré qu'avec la clé........................... correspondante

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_CD_2.png)]()

### Scénario

- Lorsque Bob veut envoyer un message de manière confidentielle à Alice, il utilise la **clé ........................... d'Alice** : Seule Alice pourra déchiffrer le message.

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_B_A.png)]()

Les système asymétriques sont utilisés pour communiquer une clé  d'un chiffrement symétrique. Cette clé est générée aléatoirement et est appelée clé de ............................ 

## Avantages

Il n'est pas nécéssaire de sécurisée la ........................... de la clé.

## Inconvénients

Le chiffrement asymétrique est une méthode ........................... et ............................ En effet, nous verrons plus tard qu'il est facile d'intercepter des messages.

## Exemples de chiffrements asymétriques

 RSA est un chifrrement asymétrique, le protocole HTTPS que nous verrons plus tard utilise un chiffrement asymétrique.

## IV/ Signature

### Scénario

- Bob correspond avec Alice et souhaite être sûr :
  - Que les messages qu'il reçoit ont bien été écrits par Alice et/ou
  - Qu'Alice a bien consulté un message.

### Problème …

- On souhaite pouvoir garantir :

  - l'authenfication d'un document : provient-il bien de son auteur ?
  - l'engagement de ce dernier à avoir consulté celui-ci

  Il s'agit du problème de la..........................., le dernier point implique que toute modification ultérieure invaliderait la signature.

la signature électronique doit être fonction du document et de son ...........................

### Mise en oeuvre 

La mise en oeuvre de cette signature profite du caractère ........................... de certains systèmes de chiffrement ..........................., comme RSA, La signature est bien attachée au document et à la personne.

Deux défauts majeurs :

- produire et vérifier la signature est long et ........................... ;
- la signature est aussi grosse que le ............................

Sans rentrer dans les détails, une fonction de **hachage** (parfaite) $h$ permet d'obtenir un condensé d'un message. Il s'agit d'une fonction :

- difficilement réversible ;
- ...........................: Si $h(m)=h(m')$, alors $m=m'$ 
Probleme: de telles fonctions n'existent pas, les collisions sont difficiles à trouver

### Signer

Lorsque Alice veut signer son message,

- elle calcule un ........................... du message $h(m)$;
- elle chiffre le condensé avec sa clé........................... $s = E_\mbox{ Kpriv}(h(m))$;
- elle envoie le message $m$, accompagné de la........................... $s$.

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_A_B_sign.png)]()

### Vérifier

Lorsque Bob veut vérifier que le message reçu provient bien d'Alice.

- il calcule un ........................... du message $h(m)$ ;
- il déchiffre la signature à l'aide de la clé ........................... de d'Alice : $D_\mbox{Kpub}(s)$;
- Si $h(m) = D_\mbox{ Kpub}(s)$, alors le message provient bien d'Alice.

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_A_B_sign.png)]()

## V/Certificat électronique

### Problème

- Lorsqu'une entité veut communiquer avec un large public, il lui suffit de diffuser sa clé ............................
- La diffusion de la clé ............................ pose néanmoins problème, puisqu'elle se fait au travers d'un canal non sécurisé.
- Que ce passe-t-il lorsque un attaquant se positionne **entre l'entité et son public** ?

### Attaque de l'homme du milieu

- Alice souhaite ............................ à Bob sa clé publique.
- L'attaquant Charlie parvient, par un moyen ou un autre, à intercepter la diffusion de la clé ............................ ;
- Il envoie à Bob sa propre clé publique.

![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_MIM_steal.png)

- L'usurpation fonctionne alors dans les deux sens :

- Si Alice signe un message, Charlie le signe avec sa clé ............................;

- Si Bob envoie un message chiffré à Alice, il utilise la ............................ de Charlie.

![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_MIM.png)

### Les certificats

Les **certificats électroniques** sont une réponse à ce problème de diffusion non sécurisée de la clé publique. Ils sont délivrés par des autorités de ............................

### Autorité de certification

- Un certificat met en jeu une autre entité : l'autorité de .............................
- Il s'agit d'organismes dûments enregistrés et certifiés auprès des autorités.............................
- Les systèmes d'exploitation et navigateurs web incluent nativement les listes des clés............................de ces autorités de certification.

### Définition

 Un certificat électronique est un ensemble de données contenant :

- des informations d'indentification (nom, email, …)
- une clé ............................
- une signature des données précédentes obtenue avec la clé............................ d'une autorité de certification.

### Création du certificat 

Lorsque Alice veut diffuser sa clé publique,

- elle effectue une demande auprès d'une ...................................................... Celle ci reçoit la clé publique et l'identité d'Alice.
- Après avoir vérifié la clé publique et l'identité d'Alice par des moyens conventionnels, elle crée un certificat contenant les données et la signature calculée avec sa clé .............................

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Certification.png)]()

### Utilisation du certificat 

Bob souhaite communiquer avec Alice. Celle-ci lui envoie son.............................

Bob peut alors vérifier l'intégrité du certificat en utilisant la clé ............................ de l'autorité de certification.

S'il authentifie le certificat, alors il peut utiliser la clé publique qu'il contient pour échanger avec Alice.

[![img](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/securisation/figs/Asym_certif_B_A.png)]()



## VI/ Le protocole HTTPS

### Rappel du protocole HTTP

Avant de parler du protocole HTTPS, petit retour sur le protocole  HTTP : un client effectue une requête HTTP vers un ............................, le serveur  va alors répondre à cette requête (par exemple en envoyant une page HTML au client). 	

Le protocole HTTP pose 2 problèmes en termes de ............................ informatique : 		

- Un individu qui intercepterait les ............................ transitant entre le  client et le serveur pourrait les lire sans aucun problème (ce qui  serait problématique notamment avec un site de e-commerce au moment où  le client envoie des données bancaires) 			

- grâce à une technique qui ne sera pas détaillée ici (le DNS spooting), un  ............................  "pirate" peut se faire passer pour un site sur lequel vous  avez l'habitude de vous rendre en toute confiance : imaginez vous voulez consulter vos comptes bancaires en ligne, vous saisissez l'adresse web de votre banque dans la barre d'adresse de  votre navigateur favori, vous arrivez sur la page d'accueil d'un site en tout point identique au site de votre banque, en toute confiance, vous  saisissez votre identifiant et votre mot de passe. C'est terminé un  "pirate" va pouvoir récupérer votre  ............................  et votre mot de passe ! Pourquoi ? Vous avez  saisi l'adresse web de votre banque comme d'habitude ! Oui, sauf que  grâce à une attaque de type "DNS spoofing" vous avez été redirigé vers  un site  ............................ , en tout point identique au site de votre banque. Dès vos identifiant et mot de passe saisis sur ce faux site, le pirate pourra  les récupérer et se rendre avec sur le véritable site de votre banque. 

### Définition

HTTPS est donc la version sécurisée de ............................ , le but de HTTPS est  d'éviter les 2 problèmes évoqués ci-dessus. HTTPS s'appuie sur le  protocole  ............................  (Transport Layer Security) anciennement connu sous le nom  de SSL (Secure Sockets Layer) 		

Les communications vont être chiffrées grâce à une clé  ............................ .  Problème : comment échanger cette clé entre le client et le serveur ?  Simplement en utilisant une paire clé publique / clé privée ! 		

### Déroulement du protocole TLS

​	Voici le déroulement des opérations : 		

- le client effectue une requête  ............................ vers le serveur, en retour le serveur envoie sa clé publique (KpuS) au client 			
- le client "fabrique" une clé K (qui sera utilisé pour chiffrer les  futurs échanges), chiffre cette clé K avec KpuS et envoie la version   ............................  de la clé K au serveur 			
- le serveur reçoit la version  ............................  de la clé K et la déchiffre  en utilisant sa clé privée (KprS). À partir de ce moment-là, le client  et le serveur sont en possession de la clé K 			
- le client et le serveur commencent à échanger des données en les  chiffrant et en les déchiffrant à l'aide de la clé K (chiffrement   ............................ ). 			

​				On peut résumer ce processus avec le schéma suivant : 		

​				![img](https://pixees.fr/informatiquelycee/n_site/img/nsi_term_secu_7.png) 				                       									

​							principe protocole TSL 	

### Déroulement du protocole HTTPS

Le protocole effectue d'abord ce qu'on appelle la " ............................ ". Il échange la clé de chiffrement à l'aide du protocole  ............................ .

Ensuite, à l'aide du protocole ............................ , les messages sont codés à l'aide de la clé K.  Le protocole AES est un protocole de chiffrement  ............................ .

### La sécurité du protocole 

Pour éviter tout attaque de type DNS Spoofing, il faut que le serveur puisse justifier de son "identité" ("voici la preuve que je suis bien le site de la banque B et pas un site "pirate""). Pour ce faire, chaque site désirant proposer des transactions  ............................ doit, périodiquement, demander (acheter dans la  plupart des cas) un  ............................  d'authentification auprès d'une  autorité de  ............................ . Nous n'allons pas entrer dans les détails du fonctionnement de ces  certificats, mais vous devez juste savoir que le  ............................  envoie ce  certificat au ............................  en même temps que sa clé  ............................  (étape 2 du  schéma ci-dessus). En cas d'absence de certificat (ou d'envoi de certificat non  conforme), le client stoppe immédiatement les ............................  avec le serveur. Il peut arriver de temps en temps que le responsable d'un site  oublie de ............................  son certificat à temps (dépasse la date  d'expiration), dans ce cas, le navigateur web côté client affichera une  page de  ............................  avec un message du style "ATTENTION le certificat  d'authentification du site XXX a expiré, il serait prudent de ne pas  poursuivre vos échanges avec le site XXXX".