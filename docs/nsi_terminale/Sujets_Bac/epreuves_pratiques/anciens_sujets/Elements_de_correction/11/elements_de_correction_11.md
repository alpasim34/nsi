# Éléments de correction Sujet 11

## Exercice 1 

Le but de cet exercice est de fournir une fonction qui renvoit une décomposition en binaire et la taille de cette décomposition du nombre passé en paramètre

```python
def conv_bin(n):
 
    bits = []

    b = n%2
    n = n//2
    bits.append(b)

    while n > 0:

        b = n%2
        n = n//2
        bits.append(b)

    bits.reverse()
    return bits, len(bits)
```

ligne 3 : initialisation de la liste bits

ligne 5 à 7 : ajout du premier bit à la liste bits

ligne 9 : tant que n est différent de 0, c'est qu'il y a des bit à ajouter

ligne 11 à 13 : ajout d'un nouveau bit à la liste bits

ligne 15 : on met les bits dans l'ordre (le bit de poids fort devant)

ligne 16 : on renvoie la liste bits et la taille de cette liste

A noter : les lignes 5 à 7 ne peuvent pas être enlevée car "déjà présente dans le while" car sinon conv_bin(0) renvoie "[], 0" au lieu de "[0], 1"


## Exercice 2 : Tri à bulles

Cet exercice est en deux partie.

La première est de compléter le code afin d'avoir une fonction effectuant un tri à bulle.

La seconde est de fournir une fonction similaire, en se basant juste sur le changement d'une ligne.

```python
def tri_bulles(T):
    n = len(T)
    for i in range(n-1, 0, -1):
        for j in range(i):
            if T[j] > T[j+1]:
                temp = T[j]
                T[j] = T[j+1]
                T[j+1] = temp
    return T


def tri_bulles2(T):
    n = len(T)
    for i in range(n-1):
        for j in range(n-2, i-1, -1):
            if T[j] > T[j+1]:
                temp = T[j]
                T[j] = T[j+1]
                T[j+1] = temp
    return T
```

La Partie 1 correspond à la fonction `tri_bulles`, la Partie 2 correspond à la fonction `tri_bulles2`.

### Partie 1

ligne 3 : range(n-1, 0, -1) : le but pour i est de redescendre. On va aller du dernier élément et redescendre jusqu'au premier (ici l'indice 1)

ligne 5 : on compare T[j] à T[j+1], les lignes suivantes servent à inverser les valeurs de T[j] et T[j+1], on a un indice

ligne 6 : il faut completer par `temp`, il sert de variable temporaire et est écrit à la ligne 8

ligne 7 : on mets la valeur de T[j+1] dans T[j], les lignes 6 à 8 servent à échanger leur valeur

### Partie 2

La seule chose à modifier et la ligne du "for j in range(...)"

En jouant avec les indices, on trouve range(n-2, i-1, -1). On commence 1 avant là où i se "termine", on s'arrête à i (on ne va pas à i-1), le tout en diminuant la valeur de j, donc avec un pas de -1