# Elements de correction Sujet 22

## Exercice 1

Le but de cet exercice est de fournir une fonction qui renvoit la liste des éléments d'une suite, allant d'un élément initial choisi jusqu'à un élément final (ici 1).

Pour information, la suite en question est la suite de Syracuse.

```python
def calcul(k):
    suite = [k]
    while suite[-1] != 1:
        n = suite[-1]
        if n % 2 == 0:
            suite.append(n//2)
        else:
            suite.append(n*3+1)
    return suite
```

Ligne 2 : On initialise une liste avec l'entier k choisi.

Ligne 3 : Tant que le dernier élément de la liste n'est pas 1, on va rentrer dans la boucle

Ligne 4 : On défini n comme étant la valeur du dernier élément de la liste 

Ligne 5 et 6 : Si n est pair, alors on ajoute la moitié de n à la liste

Ligne 7 et 8 : Si n est impair, alors on ajoute 3n+1 à la liste

Ligne 9 : On renvoie la liste, qui est notre suite

## Exercice 2

Le but de cet exercice est de compléter le code afin de vérifier pour n'importe quel mor s'il est parfait selon les règles données.

```python
def est_parfait(mot) :
    #mot est une chaîne de caractères (en lettres majuscules)
    code_c = ""
    code_a = 0
    for c in mot :
        code_c = code_c + str(dico[c])
        code_a = code_a + dico[c]
    code_c = int(code_c)
    if code_c % code_a == 0 :
        mot_est_parfait = True
    else :
        mot_est_parfait = False
    return [code_a, code_c, mot_est_parfait]
```

Ligne 4 : On initialise le code additionné à 0. On va effectuer une somme, toutes les sommes commencent à 0

Ligne 6 : On concatène le code_c avec la chaine de caractère correspondant à l'entier de la lettre actuelle

Ligne 7 : On ajoute l'entier correspondant à la lettre actuelle à notre code_a

Ligne 9 : On vérifie si code_a divise code_c. Si oui le mot est parfait, si non il ne l'es pas 