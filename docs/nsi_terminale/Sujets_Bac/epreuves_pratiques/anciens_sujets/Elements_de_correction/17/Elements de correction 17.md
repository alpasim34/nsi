# Elements de correction

## Exercice 1

Le but de cet exercice est de trouver le minimum d'un tableau et de renvoyer son indice. 2 solutions sont possibles: en utilisant les méthodes python ou en parcourant le tableau.

```python
def RechercheMin(tab):
    mini=min(tab)
    return tab.index(mini)
```

ligne 1: stockage du minimum du tableau dans une variable mini

ligne 2: renvoie de l'indice de mini

```python
def RechercheMin(tab):
    indmin=0
    for i in range(1,len(tab)):
        if tab[i]<tab[indmin]:
            indmin=i
     return indmin
```

ligne 1: initialisation de indmin à 0,, ici indmin est l'indice du minimum on considère donc au départ que le minimum de la liste est son premier élément

ligne 2: parcours des éléments de la liste à partir du deuxième

ligne 3:  si l'élément d'indice i est plus petit que celui d'indice indmin

ligne 4: indmin devient i

ligne 5: renvoie de indmin 



## Exercice 2

Le but de cet exercice est de faire un tri tout en partionnant une liste en 3 parties(la première composée uniquement de 0, la seconde non triée, la troisième composée uniquement de 1). Il fait travailler les indices dans une liste.

```python
def tri(tab):
    i= 0
    j= len(tab)-1
    while i != j :
        if tab[i]== 0:
            i= i+1
        else :
            tab[i],tab[j] = tab[j],tab[i]
            j= j-1
    return tab
```

ligne 1: initialisation de i à 0,i est le premier indice de la zone non triee

ligne 2: initialisation de j au dernier indice du tableau, j lest e dernier indice.de la zone non triee, au debut, la zone non triee est le tableau entier.

ligne 3: tant que i est différent de j

ligne 4: si l'élément d'indice i est égale à 0, ici l'élément est donc à la bonne place

ligne 5: incrémentation  de 1 de i

ligne 6: sinon, on est donc dans le cas ou l'élément d'indice i est égale à 1

ligne 7: échange des valeurs d'indice i et d'indice j

ligne 8: on décrement j de 1, la liste trié de 1 est plus grande

ligne 9: on renvoie le tableau trié

 