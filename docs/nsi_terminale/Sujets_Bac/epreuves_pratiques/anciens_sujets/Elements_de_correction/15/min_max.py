def RechercheMinMax(tab):
    if len(tab)>0:
        mini=min(tab)
        maxi=max(tab)
    else:
        mini=None
        maxi=None
    res=dict()
    res['min']=mini
    res['max']=maxi
    return res

print(RechercheMinMax([5, 1, -3, 10, 2]))
print(RechercheMinMax([]))