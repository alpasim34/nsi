# Elements de correction

## Exercice 1

Le but de cet exercice est de retrouver les indices d'un élément dans un tableau. Cet exercice a pour but d'utiliser le parcours de liste ainsi que l'indexation.

```python
def recherche(elt,tab):
    res=[]
    for i in range(len(tab)):
        if tab[i]==elt:
            res.append(i)
    return res
```

ligne 1: initialisation d'une variable res à une liste vide

ligne 2: parcours des éléments de la liste, i étant l'indice de parcours

ligne 3: si l'élément d'indice i est elt

ligne 4: rajout de i à res

ligne 5: renvoie de res

## Exercice 2

Le but de cet exercice est de renvoyer la moyenne d'un élève. Cet exercice permet d'utiliser les dictionnaires avec les associations cle/valeur.

```python
resultats = {'Dupont':{'DS1' : [15.5, 4],
                       'DM1' : [14.5, 1],
                       'DS2' : [13, 4],
                       'PROJET1' : [16, 3],
                       'DS3' : [14, 4]},
             'Durand':{'DS1' : [6 , 4],
                       'DM1' : [14.5, 1],
                       'DS2' : [8, 4],
                       'PROJET1' : [9, 3],
                       'IE1' : [7, 2],
                       'DS3' : [8, 4],
                       'DS4' :[15, 4]}}


def moyenne(nom):
    if nom in resultats:
        notes = resultats[nom]
        total_points = 0
        total_coefficients = 0
        for valeurs  in notes.values():
            note , coefficient = valeurs
            total_points = total_points + note * coefficient
            total_coefficients = total_coefficents + coefficient
        return round( total_points / total_coefficients , 1 )
    else:
        return -1
```

ligne 1: test d'appartenance du nom dans le dictionnaire

ligne 2: stockage des notes de l'élève dans la variable notes

ligne 3: initialisation de total_points à 0

ligne 4: initialisation de total_coefficients à 0

ligne 5: parcours de chacune des valeurs du dictionnaire de l'élève

ligne 6: affectation à note et coefficients des valeurs du devoir que l'on regarde actuellement .

ligne 7; ajout des points coefficientés à total_points

ligne 8: ajout du coefficient à total_points

ligne 9: renvoie de la moyenne avec un chiffre après la virgule

ligne 10: sinon, on est donc dans le cas ou l'élève n'est pas présent dans le dictionnaire.

ligne 11: renvoie de -1