# Eléments de correction 1

## Exercice 1

```python
def recherche(tab,n):
    res=len(tab)
    for i in range(len(tab)):
        if tab[i]==n:
            res=i
    return res
```

Le but de cet exercice est de rechercher la dernière occurrence d'un élément dans une liste si elle est présente et la longueur de la liste sinon. Cet exercice fait travailler le parcours de liste et la sélection d'élément à l'aide d'indice dans une liste.

ligne 1: initialisation  du résultat à la longueur de la liste, le résultat ne changera pas si l'élément n n'est pas présent dans la liste 

ligne 2: parcours des éléments de la liste par leur indice

ligne 3: si l'élément à l'indice i est égale à l'élément n

ligne 4: le résultat est égale à l'indice de l'élément dans la liste égale à n

ligne 5: renvoie du résultat  

## Exercice 2

```python
from math import sqrt   # import de la fonction racine carrée

def distance(point1, point2): 
    """ Calcule et renvoie la distance entre deux points. """
    return sqrt((point1[0]-point2[0])**2 + (point1[1]-point2[1])**2)

assert distance((1, 0), (5, 3)) == 5.0, "erreur de calcul"

def plus_courte_distance(tab, depart):
    """ Renvoie le point du tableau tab se trouvant à la plus     
    courte distance du point depart."""
    point = tab[0]
    min_dist = distance(depart,point)
    for i in range (1, len(tab)):
        if distance(tab[i], depart)<min_dist:
            point = tab[i]
            min_dist = distance(tab[i], depart)
    return point

assert plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0)) == (2, 5), "erreur"
```

Ici le sujet comporte deux erreurs, la  première phrase est fausse: On souhaite programmer une fonction donnant le point à la distance la plus proche entre un point de départ et une liste de points.

La derniere assertion est également fausse: un point est sous la forme d'un tuple de deux entiers et non d'une liste de deux entiers.

 ### la fonction distance

la première parenthèse doit contenir x-x' 

x est le premier élément du point donc point[0]

la seconde parenthèse doit contenir y-y' 

 y est le second élément du point donc point[1]



### la fonction plus courte distance

ligne 1: on initialise point au premier point de la liste, ici la variable point représente le point le plus proche

ligne 2: initialisation de min_dist comme la distance entre le départ et le point 

ligne 3: parcours des éléments de la liste excepté le premier qui a déjà été analyser lors de l'initialisation

ligne 4: si la distance entre le point en cours et le départ est plus petite que la distance minimale enregistrée

ligne 5: point= point en cours

ligne 6 : la distance minimale est la distance entre le point en cours et le point de départ

ligne 7: renvoie du  point 

