# Elements de correction sujet 6

## Exercice 1

```python
def rendu(somme_a_rendre):
    n1=0
    n2=0
    n3=0
    while somme_a_rendre>=5:
        n1+=1
        somme_a_rendre-=5
    while somme_a_rendre>=2:
        n2+=1
        somme_a_rendre-=2
    n3=somme_a_rendre
    return [n1,n2,n3]
```

Le but de cet exercice est de simuler un algorithme glouton sur le rendu de monnaie.

Ligne 1 à 3: initialisation des variables n1,n2 et n3 à 0

Ligne 4:  boucle "tant que la somme à rendre est supérieur ou égale à 5"

ligne 5: incrémentation de 1 de n1

ligne 6: retrait de 5 à la somme à rendre

ligne 7: boucle "tant que la somme à rendre est supérieur ou égale à 2"

ligne 8: incrementation de 1 de n2

ligne 9: retrait de 2 de la somme à rendre

ligne 10: n3 est égale au reste de la somme à rendre il peut etre égale à 0 ou 1

ligne 11: renvoie de la liste n1,n2,n3

## Exercice 2

```python
class Maillon :
    def __init__(self,v,suivant=None) :
        self.valeur = v
        self.suivant = suivant

class File :
    def __init__(self) :
        self.dernier_file = None

    def enfile(self,element) :
        nouveau_maillon = Maillon(element, self.dernier_file)
        self.dernier_file = nouveau_maillon

    def est_vide(self) :
        return self.dernier_file == None

    def affiche(self) :
        maillon = self.dernier_file
        while maillon != None:
            print(maillon.valeur)
            maillon = maillon.suivant

    def defile(self) :
        if not self.est_vide() :
            if self.dernier_file.suivant == None :
                resultat = self.dernier_file.valeur
                self.dernier_file = None
                return resultat
            maillon = self.dernier_file
            while maillon.suivant.suivant != None :
                maillon = maillon.suivant
            resultat = maillon.suivant.valeur
            maillon.suivant = None
            return resultat
        return None 
```

Le but de cet exercice est de travailler la POO en python.

Ici il y a d'abord une erreur dans la classe maillon qui dans son constructeur doit prendre 2 paramètres. C'est pourquoi le paramètre suivant à été rajouté dans le constructeur.



 ### La méthode enfiler

ligne 1: Création dun nouveau maillon avec pour tête le nouvel élement

ligne 2: affectation a l'attribut dernier_file le nouveau maillon

### la méthode affiche

ligne 1:stockage dans maillon de la file

ligne 2: tant que le maillon n'est pas nul

ligne 3: affichage de la valeur du maillon

ligne 4:  affectation à maillon du maillon suivant



### la méthode défile 

ligne1:  si la file n'est pas vide

ligne 2: si la file qu'un élément

ligne 3 affectation à resultat de la valeur du maillon en tete de file

ligne 4: la file est donc égale à None car elle ne contient plus d'élément

ligne 5: renvoie de la valeur de la tete de la liste 

ligne 6: s on est ici dans le cas ou la file contient plus de 2 éléments  ainsi on affecte à maillon la valeur de la file 

ligne 7: tant que maillon n'est pas de taille 3

ligne 8: maillon est égale au maillon suivant

ligne 9: resultat est égale à la valeur du maillon suivant

ligne 10: maillon suivant est égale à None

ligne 11: renvoyer le resultat

ligne 12: on est ici dans le cas ou la file est vide on renvoie donc None










