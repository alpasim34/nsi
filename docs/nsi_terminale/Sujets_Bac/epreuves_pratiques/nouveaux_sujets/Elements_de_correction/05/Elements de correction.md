# Elements de correction

## Exercice 1

``` python
def RechercheMinMax(tab):
    if len(tab)>0:
        mini=min(tab)
        maxi=max(tab)
    else:
        mini=None
        maxi=None
    res=dict()
    res['min']=mini
    res['max']=maxi
    return res

```

Cet exercice a pour but l'utilisation des fonctions python min et max sur les listes ainsi que la création et la manipulation de dictionnaire afin d'extraire le minimum et le maximum d'une liste.

Une subtilité est ici présente dans le cas où la liste est vide et ainsi les valeurs maximum et minimum doivent avoir la valeur None.

 ligne 2: test de vacuité de la liste passée en paramètre

ligne 3: recherche du minimum et stockage dans la variable mini à l'aide de la fonction min

ligne 4: recherche du maximum et stockage dans la variable maxi à l'aide de la fonction max

ligne 5 à 7: initialisation à None de mini et maxi

ligne 8: création d'un dictionnaire vide

ligne 9 et 10: remplissage du dictionnaire

ligne 11: renvoie du dictionnaire

## Exercice 2

``` python
import random

class Carte:
    """Initialise Couleur (entre 0 à 3), et Valeur (entre 1 à 13)"""
    def __init__(self, c, v):
        self.Couleur = c
        self.Valeur = v

    """Renvoie le nom de la Carte As, 2, ... 10,
       Valet, Dame, Roi"""
    def getNom(self):
        if ( self.Valeur > 1 and self.Valeur < 11):
            return str( self.Valeur)
        elif self.Valeur == 11:
            return "Valet"
        elif self.Valeur == 12:
            return "Dame"
        elif self.Valeur == 13:
            return "Roi"
        else:
            return "As"

    """Renvoie la couleur de la Carte (parmi pique, coeur, carreau, trefle"""
    def getCouleur(self):
        return ['pique', 'coeur', 'carreau', 'trefle' ][self.Couleur]

class PaquetDeCarte:
    def __init__(self):
        self.contenu = []

    """Remplit le paquet de cartes"""
    def remplir(self):
        for couleur in range(4):
            for valeur in range(1,14):
                carte=Carte(couleur,valeur)
                self.contenu.append(carte)
        random.shuffle(self.contenu)


    """Renvoie la Carte qui se trouve à la position donnée"""
    def getCarteAt(self, pos):
        return self.contenu[pos]
```

Une erreur est glissée dans le sujet la couleur d'une carte à une valeur entre 0 et 3.

Le but de cet exercice est de travailler la programmation orientée objet,l'utilisation de boucle, l'utilisation de méthode python ainsi que l'accès dans une liste.



La méthode remplir crée un paquet de carte contenant toutes les cartes dans chacune des couleurs possibles.

Pour créer ces cartes il va falloir réaliser une double boucle qui pour chaque couleur et pour chaque valeur possible créera une carte et l'ajoutera dans le paquet.

Ensuite, il faudra mélanger le paquet.

ligne 2: parcours des couleurs, ici les couleurs ont une valeur entre 0 et 3
ligne 3: parcours des valeurs, ici les couleurs ont une valeur entre 1 et 13

ligne 4: Création de la carte

Ligne 5: ajout de la carte dans le paquet

ligne 6: mélange des cartes



La méthode getCarteAt(pos) permet d'accéder à la carte à la position pos dans le paquet.

Pour cela on utilise tout simplement l'accès à l'élément dans une liste à l'aide de son indice et donc l'utilisation des [].
