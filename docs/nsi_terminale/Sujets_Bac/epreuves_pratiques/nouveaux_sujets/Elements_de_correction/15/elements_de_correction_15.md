# Éléments de correction Sujet 15

## Exercice 1 

Le but de cet exercice est de fournir une fonction qui le nombre de répétitions d'un élément dans un tableau

```python
def nb_repetitions(elt, tab):
    nb = 0
    for e in tab:
        if elt == e:
            nb += 1
    return nb
```

ligne 1 : initialisation à 0 de notre compteur

ligne 2 à 4 : Pour chaque e dans le tableau,
		si notre élément est équivalent à e, 
		augmente notre compteur de 1


## Exercice 2

Le but de cet exercice est de compléter une fonction permettant d'obtenir la conversion d'un entier positif en binaire

```python
def binaire(a):
    bin_a = str(a%2)
    a = a // 2
    while a > 0 :
        bin_a = str(a%2) + bin_a
        a = a // 2
    return bin_a
```

ligne 1 : Le dernier bit de bin_a sera a%2 (donc 0 ou 1)

ligne 3 : On exécute la boucle tant que a est supérieur à 0

ligne 4 : On prolonge la chaine de caractère bin_a par concaténation successives. str(x) permet de convertir un entier x en chaine de caractère

ligne 5 : On réduit a en le divisant par 2