# Éléments de correction Sujet 18

## Exercice 1 

Le but de cet exercice est de fournir une fonction qui renvoie le plus petit élément d'une liste et celui avec le même indice dans une seconde liste

```python
def mini(t_moy, annees):
    mini = 0
    i = 0
    while i < len(t_moy):
        if t_moy[mini] > t_moy[i]:
            mini = i
        i += 1
    return t_moy[mini], annees[mini]
```

ligne 1 : initialisation à 0 de notre indice pour l'élément minimal
ligne 2 : initialisation à 0 de notre indice pour parcourir le tableau

ligne 3 : Tant que i est plus petit que la taille de t_moy
ligne 4 : si notre élément minimal est plus grand que notre élément d'indice i
ligne 5 : alors l'indice de l'élément minimal devient i
ligne 6 : on incrémente i

ligne 7 : on renvoie la température minimale et l'année correspondante
		

## Exercice 2

Le but de cet exercice est de compléter plusieurs fonction afin d'avoir un moyen de vérifier qu'un nombre est un palindrome

```python
def inverse_chaine(chaine):
    result = ''
    for caractere in chaine:
       result = caractere + result
    return result

def est_palindrome(chaine):
    inverse = inverse_chaine(chaine)
    return chaine == inverse
    
def est_nbre_palindrome(nbre):
    chaine = str(nbre)
    return est_palindrome(chaine)
```

fonction 1 ligne 1 : On initialise notre chaine result à la chaine vide ( '' )

fonction 1 ligne 3 : on concatène chaque caractère de notre chaine à résult, l'ordre de concaténation est important car dans un sens on obtient une copie de la chaine, dans l'autre on obtient son inverse

fonction 2 ligne 2 : on renvoie un booleen. Il est obtenu en comparant notre chaine à son inverse

fonction 3 ligne 1 : on convertit notre nombre en chaine de caractère grâce à str()