 # Sujet 3

## Exercice 1

Le but de cette exercice est d'utiliser les indices dans les tableaux.

On obtient l'élément d'indice i du nouveau tableau, en soustrayant l'élement d'indice i-1 à l'élement d'indice i du tableau passé en paramètre.

```python
def delta(tab):
    res=[tab[0]]
    for i in range(1,len(tab)):
        elt=tab[i]-tab[i-1]
        res.append(elt)
    return res
```

ligne 1 : création d'un nouveau tableau contenant le premier élément du tableau passé en paramètre.

ligne 2: parcours des élements du tableau à partir du 2 ème

ligne 3: calcul de la valeur du nouvel élément d'indice i

ligne 4: ajout de l'élément dans le tableau res

ligne 5 : renvoi du tableau res

## Exercice 2

le but de cet exercice est de réaliser un parcours en profondeur infixe sur un arbre binaire.

On parcours ainsi le sous arbre gauche puis la racine et enfin le sous arbre droit.

```python
class Noeud:
    def __init__(self, g, v, d):
        self.gauche = g
        self.valeur = v
        self.droit = d

    def __str__(self):
        return str(self.valeur)

    def est_une_feuille(self):
        '''Renvoie True si et seulement si le noeud est une feuille'''
        return self.gauche is None and self.droit is None


def expression_infixe(e):
    s = ''
    if e.gauche is not None:
        s = s + expression_infixe(e.gauche)
    s = s + str(e.valeur)
    if e.droit is not None:
        s = s + expression_infixe(e.droit)
    if e.est_une_feuille():
        return s

    return '('+ s +')'

## Programme principal

e = Noeud(Noeud(Noeud(None, 3, None), '*', Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))), '-', Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None)))

print(expression_infixe(e))

```

ligne 1: on initialise s à une chaine de caractère vide, s étant le résultat que l'on va renvoyer

ligne 2: si le sous arbre gauche n'est pas vide

ligne 3: ajout à s  du parcours du sous abre gauche

ligne 4 ajout à s de la racine, parcours de la racine

ligne 5: si le sous arbre droit n'est pas vide

ligne 6: ajout à s du parcours du sous arbre droit

ligne 7: si l'arbre est une feuille

ligne 8: il n'y a donc plus rien à parcourir, on renvoi donc s

ligne 9 : ajout des parenthése et renvoie de s
