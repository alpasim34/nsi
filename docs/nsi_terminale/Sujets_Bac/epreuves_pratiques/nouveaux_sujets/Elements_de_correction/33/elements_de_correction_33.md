# Éléments de correction Sujet 33

## Exercice 1 

Le but de cet exercice est de fournir une fonction qui convertit une liste de bits en un entiers

```python
def convertir(T):
    res = 0
    for bit in T:
        res = res*2 + bit
    return res
```

ligne 1 : initialisation à 0 de res
ligne 2 : pour chaque bit du tableau T
ligne 3 : on multiplie res par 2 et on lui ajoute le bit actuel
ligne 4 : on renvoie res
		

## Exercice 2

Le but de cet exercice est de compléter la fonction tri_insertion 

```python
def tri_insertion(L):
    n = len(L)

    # cas du tableau vide
    if n == 0:
        return L

    for j in range(1,n):
        e = L[j]
        i = j

        # A l'etape j, le sous-tableau L[0,j-1] est trie
        # et on insere L[j] dans ce sous-tableau en determinant
        # le plus petit i tel que 0 <= i <= j et L[i-1] > L[j].
        while  i > 0 and L[i-1] > L[j]:
            i = i-1
        
        # si i != j, on decale le sous tableau L[i,j-1] d'un cran
        # vers la droite et on place L[j] en position i
        if i != j:
            for k in range(j,i,-1):
                L[k] = L[k-1]
            L[i] = e
    return L
```

trou 1 : C'est le cas du tableau vide, on a mis la taille de L dans n juste avant, on a juste à vérifier si n est égal à 0

trou 2 et 3 : on décrémente i tant que i est plus grand que 0 et que L[i-1] est plus grand que L[j]

trou 4 : j est plus grand que i, on veut que les k aillent de j à i, on doit incrémenter de -1 (ou décrémenter de 1, c'est la même chose)

trou 5 : on remplace chaque L[k] par l'élément situé juste avant
trou 6 : on mets e à l'indice i