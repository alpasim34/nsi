# Elements de correction Sujet 22

## Exercice 1

Le but de cet exercice est de fournir une fonction qui renvoit le nombre d'occurence de chaque caractère d'une phrase dans un dictionnaire

```python
def occurence_lettres(phrase):
    occ = {}
    for lettre in phrase:
        if lettre in oc:
            occ[lettre] = occ[lettre] + 1
        else:
            occ[lettre] = 1
    return occ
```

Ligne 2 : On initialise un dictionnaire pour compter les occurences des lettres dans la phrase

Ligne 3 : On va parcourir toutes les lettres de la phrase

Ligne 4 et 5 : Si la lettre actuelle est déjà dans notre dictionnaire, alors on augmente son nombre d'occurence de 1

Ligne 6 et 7 : Si la lettre n'est pas dans notre dictionnaire, alors on initialise son nombre d'occurence à 1. La lettre est désormais dans le dictionnaire

Ligne 8 : On renvoie notre dictionnaire d'occurence

## Exercice 2

Le but de cet exercice est de compléter le code afin d'obtenir la fusion de deux listes (tel utilisé dans le tri fusion par exemple) sans utiliser de récursion. L1 et L2 sont trié, on renvoie L12 qui contient tout les élément de L1 et L2 et eux aussi triés.

```python
def fusion(L1,L2):
    n1 = len(L1)
    n2 = len(L2)
    L12 = [0]*(n1+n2)
    i1 = 0
    i2 = 0
    i = 0
    while i1 < n1 and i2 < n2 :
        if L1[i1] < L2[i2]:
            L12[i] = L1[i1]
            i1 = i1 + 1
        else:
            L12[i] = L2[i2]
            i2 = i2 + 1
        i += 1
    while i1 < n1:
    	L12[i] = L1[i1]
    	i1 = i1 + 1
    	i = i + 1
    while i2 < n2:
    	L12[i] = L2[i2]
    	i2 = i2 + 1
    	i = i + 1
    return L12
```

Ligne 3 : On crée une liste de n1+n2 éléments afin de récupérer tout les éléments de L1 et L2

Ligne 7 : La boucle continue tant que i1 n'est pas égal à n1 ou que i2 n'est pas égal à n2. les i sont les indices, n sont les tailles des listes.

Ligne 9 : L1[i1] est plus petit que L2[i2], on va donc le mettre à l'indice i de L12.

Ligne 10 : on augmente i1 de 1 car on a rangé L1[i1], il faut désormais ranger à L1[i1 + 1]

Ligne 12 : L2[i2] est plus petit, on va donc le ranger dans L12

Ligne 13 : on augmente i2 de 1 car on a rangé L2[i2], il faut désormais ranger à L2[i2+ 1]

Ligne 16 à 18 : Tout les éléments de L2 ont été rangé, on va ranger tout les éléments de L1

Ligne 20 à 22 : Tout les éléments de L1 ont été rangé, on va ranger tout les éléments de L2