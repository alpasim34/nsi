# Sujet 30 

## Exercice 1

Le but de cet exercice est réaliser la fonction de fusion utilisée dans le tri_fusion. 

```python
def fusion(L1,L2):
    n1 = len(L1)
    n2 = len(L2)
    res = []
    i1 = 0
    i2 = 0
    while i1 < n1 and i2<n2 :
        if L1[i1] < L2[i2]:
            res.append(L1[i1])
            i1 = i1+1
        else:
            res.append(L2[i2])
            i2 = i2+1
    while i1 < n1:
        res.append(L1[i1])
        i1 = i1 + 1
        
    while i2 < n2:
        res.append(L2[i1])
        i2 = i2 + 1
       
    return res

```

ligne 1: affection à n1 de la longueur de L1
ligne 2: affectation  à n2 de la longueur de L2

ligne 3: création d'une liste vide pour la variable res

ligne 4: affectation à i1 de 0, i1 sera l'indice de parcours dans L1

ligne 5: affectation à i2 de 0, i2 sera l'indice de parcours dans L2

ligne 6: tant que i1 est inférieur à n1 et i2 est inférieur à n2

ligne 7: test si l'élément d'indice i1 dans l1 est inférieur à l'élément d'indice i2 dans l2

ligne 8: ajout de l'élément d'indice i1 de L1 à la fin de res

ligne 9: incrémentation de i1

ligne 10: sinon , on est dans la cas ou l'élément d'indice i1 dans l1 est supérieur ou égal à l'élément d'indice i2 dans l2

ligne 11: ajout de l'élément d'indice i2 de L2 à la fin de res

ligne 12: incrémentation de i2

ligne 13: arrivé ici soit, i1=n1, soit i2=n2. Tant que i1 est inférieur à n1

ligne 14: ajout de l'élément d'indice i1 de L1 à la fin de res

ligne 15: incrémentation de i1

ligne 16: Tant que i2 est inférieur à n2

ligne 17: ajout de l'élément d'indice i2 de L2 à la fin de res

ligne 18: incrémentation de i2

ligne 19: renvoi du résultat



## Exercice 2

Le but de cet exercice est l'accés à des éléments d'un dictionnaire ainsi que de travauller la récursivité

```python
def rom_to_dec (nombre):

    """ Renvoie lâ€™Ã©criture dÃ©cimale du nombre donnÃ© en chiffres romains """

    dico = {"I":1, "V":5,"X":10,"L":50,"C":100,"D":500,"M":1000}
    if len(nombre) == 1:
        return dico[nombre]

    else:
        ### on supprime le premier caractÃ¨re de la chaÃ®ne contenue dans la variable nombre
        ### et cette nouvelle chaÃ®ne est enregistrÃ©e dans la variable nombre_droite
        nombre_droite = nombre[1:]
           
        if dico[nombre[0]] >= dico[nombre[1]]:
            
            return dico[nombre[0]] + rom_to_dec (nombre_droite)
        else:
         
            return rom_to_dec (nombre_droite)- dico[nombre[0]]

```

ligne 1: création du dictionnaire 

ligne 2: test si la taille du nombre est égale à un

ligne 3 : renvoie de la valeur du nombre dans le dico

ligne 4: sinon, on est donc dans le cas ou le nombre à une taille d'au moins deux 

ligne 5: affectation à nombre_droite du nombre privée de son premiere element 

ligne 6:test si la valeur dans le dictionnaire du premier élément du nombre est superieur ou égale à la valeur dans le dictionnaire du second élément du nombre

ligne 7: renvoi de la valeur du premier élement du nombre +  la valeur de la convertion du nombre privé de son premier element 

ligne 8: sinon, on est donc dans le cas ou le premier élément du nombre est plus petit que le suivant

ligne 9: renvoi de la valeur de la conversition du nombre privé de son premier élément - la valeur du premier élément