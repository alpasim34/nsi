# **TP Arbres binaires**

## **Parcours d'arbres**

Parcourir un arbre consiste à visiter chacun des noeuds de l'arbre, à effectuer un traitement sur chacune des étiquettes des noeuds de l'arbre.

On distingue différents parcours selon l'ordre dans lequel les noeuds seront visités : des parcours en **profondeur** et en **largeur**.

## Parcours en largeur

Un parcours en largeur visite les noeuds d'un arbre niveau par niveau : le noeud racine de profondeur nulle, les noeuds de profondeur 1, puis les noeuds de profondeur 2, etc...

Pour une profondeur donnée, on visite les noeuds de gauche à droite.

Le principe est que lors de la visite du niveau de profondeur *i*, on mémorise dans une structure ad hoc les noeuds fils, qui sont donc des noeuds de profondeur *i+1* qui seront à visiter une fois la visite de la profondeur *i* terminée.

La structure dans laquelle mémoriser les noeuds fils doit permettre :
- d'ajouter un élément (un noeud) à la structure
- de récupérer un élément ; plus précisément de récupérer l'élément le plus anciennement inséré
- de tester que la structure est vide (fin de l'algorithme)

On reconnaît là l'interface du type de données abstrait file qui met en œuvre le principe FIFO : premier arrivé, premier sorti.

Pour la réalisation de notre parcours en largeur, les éléments de cette file sont des arbres : l'arbre à parcourir, puis l'arbre fils gauche de sa racine, puis l'arbre fils droit de sa racine, etc...

### **Parcourir en largeur**

**Question 1** - Remplir la Docstring de la fonction suivante :

```python
def etiquettes_file(exp):
    """
    :parametre exp: ...
    :renvoie: ...

    description...
    """
    if exp.is_leaf():
        return [exp.get_data()]
    else:
        res = [exp.get_data()]
        fil = File()
        fil.enfile(exp.get_left_subtree())
        fil.enfile(exp.get_right_subtree())
        while not fil.est_vide():
            tmp = fil.defile()
            if not tmp.is_leaf():
                fil.enfile(tmp.get_left_subtree())
                fil.enfile(tmp.get_right_subtree())
            res += [tmp.get_data()]
        return res
```

### **Parcourir avec une pile**

**Question 2** - Que remarque t'on si l'on remplace la file dans l'algorithme précédent par une pile ?
