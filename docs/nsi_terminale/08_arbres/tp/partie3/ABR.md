# **TP Arbres binaires**

## **Arbres de recherche**

Un **arbre de recherche**, ABR, est un arbre binaire qui va être utilisé pour réaliser « efficacement » des opérations de recherche d'une valeur, mais aussi des opérations d'insertion et suppression de valeurs.

Les valeurs des étiquettes de l'arbre doivent donc appartenir à un ensemble ordonné.

## Caractériser les arbres binaires de recherche

#### **-> Definition**  - Arbre binaire de recherche – ABR

Un arbre binaire de recherche est un arbre binaire tel que pour tout noeud d'étiquette ```e``` :
- les étiquettes de tous les noeuds de son sous-arbre gauche sont inférieures ou égales à ```e```
- les étiquettes de tous les noeuds de son sous-arbre droit sont supérieures à ```e```.

### **Des arbres binaires de recherche**

**Question 1** - Indiquez quels sont parmi les arbres suivants ceux qui sont des arbres binaires de recherche.

![abr_1](./abr_1.png) ![abr_2](./abr_2.png) ![abr_3](./abr_3.png)

### **Les plus petite et plus grande étiquettes**

**Question 2** - Dans quel noeud d'un arbre binaire de recherche se trouve la plus petite étiquette ? La plus grande ?

### **Parcours ordonné**

**Question 3** - Lequel des parcours classiques d'arbres binaires permet de visiter les noeuds d'un ABR dans l'ordre des étiquettes ?

### **Reconnaître un arbre binaire de recherche**

**Question 4** - Definir les 3 arbres binaires de la **question 1**.

La fonction suivante prend en parametre un arbre binaire et renvoie la liste des étiquettes de ce dernier. La fonction suit un parcours en profondeur infixé.

```python
def parcours_infixe(a):
    if a.is_leaf():
        return [a.get_data()]
    elif a.get_left_subtree().is_empty():
        if a.get_right_subtree().is_empty():
            return [a.get_data()]
        else:
            return [a.get_data()] + parcours_infixe(a.get_right_subtree())
    else:
        if a.get_right_subtree().is_empty():
            return parcours_infixe(a.get_left_subtree()) + [a.get_data()]
        else:
            return parcours_infixe(a.get_left_subtree()) + [a.get_data()] + parcours_infixe(a.get_right_subtree())
```

**Question 5** - Proposez un prédicat (une fonction) qui reconnaît si un arbre binaire donné est un arbre binaire de recherche.

Une piste possible pour cela serai de réaliser un parcours de l'arbre qui devrait visiter les noeuds dans l'ordre croissant, et vérifier que les étiquettes sont bien dans l'ordre croissant. (On supposera qu'il n'y a pas de doublons dans l'arbre)

## Recherche d'une valeur dans un arbre binaire de recherche

Lorsque l'on recherche une valeur dans un ABR, on regarde dans un premier temps la valeur de la racine et nous avons 3 situations possibles :
- la valeur de la racine est égal à celle recherchée -> Fin de l'algorithme
- la valeur de la racine est suppérieur à celle recherchée -> on regarde dans le fils gauche de la racine
- la valeur de la racine est inférieur à celle recherchée -> on regarde dans le fils droit de la racine

**Question 6** - Proposez une fonction Python qui renvoie le noeud d'un arbre binaire de recherche dont l'étiquette est égale à une valeur donnée. La fonction pourra retourner ```None``` si la valeur n'était pas présente dans l'arbre.

## Insertion d'une valeur dans un arbre binaire de recherche

L'insertion d'une valeur *v* dans un arbre binaire de recherche peut reposer sur le déroulé récursif suivant :

- si l'arbre est vide, renvoyer l'arbre arbre constitué d'une unique feuille portant la valeur *v*
- si la valeur à insérer *v* est inférieure ou égale à la valeur de la racine, renvoyer l'arbre formé de
    - la racine
    - l'arbre formé du fils gauche dans lequel aura été inséré la valeur *v*
    - le fils droit
- sinon la valeur *v* est supérieur à la valeur de la racine, renvoyer l'arbre formé de
    - la racine
    - le fils gauche
    - l'arbre formé du fils droit dans lequel aura été inséré la valeur *v*

### **Insérer une valeur dans un ABR**

**Question 7** - Proposez une fonction Python qui renvoie un arbre binaire de recherche donné augmenté d'une valeur donnée.
