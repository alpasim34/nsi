# **TP Arbres binaires**

## **Parcours d'arbres**

Parcourir un arbre consiste à visiter chacun des noeuds de l'arbre, à effectuer un traitement sur chacune des étiquettes des noeuds de l'arbre.

On distingue différents parcours selon l'ordre dans lequel les noeuds seront visités : des parcours en **profondeur** et en **largeur**.

## Parcours en profondeur

### **Trois parcours en profondeur**

Le principe des parcours en profondeur est de visiter récursivement les noeuds de l'arbre.

On distingue trois parcours, en fonction de comment l'on traite l'étiquette d'un noeud :

- avant la visite de ses fils gauche et droit
- après la visite de son fils gauche (et avant la visite de son fils droit)
- après la visite des ses fils gauche et droit

Pour désigner ces trois parcours en profondeur, on utilise respectivement les termes :

- préfixe
- infixe
- postfixe

### **Préfixe, infixe, et postfixe**

**Question 1** - Ecrire sur **papier** le résultat de ces 3 fonctions puis vérifier les résultats en testant sur ordinateur.

```python
def afficher_A(a):
    if not a.is_empty():
        afficher_A(a.get_left_subtree())
        print(a.get_data(), end="")
        afficher_A(a.get_right_subtree())

def afficher_B(a):
    if not a.is_empty():
        afficher_B(a.get_left_subtree())
        afficher_B(a.get_right_subtree())
        print(a.get_data(), end="")

def afficher_C(a):
    if not a.is_empty():
        print(a.get_data(), end="")
        afficher_C(a.get_left_subtree())
        afficher_C(a.get_right_subtree())
```

sur l'arbre ci-dessous.

![arbre](./arbre.png)

**Question 2** - Associez le terme adéquat parmi **préfixe**, **infixe** et **postfixe** à chacune des trois fonctions d'affichage.

### **Représentation d'expressions arithmétiques**

Une expression arithmétique construite avec des opérateurs binaires (addition, soustraction, multiplication, division), peut être représentée par un arbre binaire dont les noeuds internes portent les opérateurs et les feuilles des symboles de variables (x, y, z...), ou des constantes (1, 20, 42, etc...).

Ainsi, l'arbre suivant représente l'expression ```6(x+y)+(y−14)``` :

![expression](./expression.png)

**Question 3** - Ecrire sur papier les expressions suivantes sous forme d'arbre binaire : ```6*(x+y)+(y−14)```, ```(x+5)*(y/2)``` et ```4*(x-1)``` puis les définir sur python.

### **Représentation d'une expression**

**Question 4** - Proposez une fonction qui accepte en paramètre un arbre représentant une expression arithmétique, et renvoie cette expression.
