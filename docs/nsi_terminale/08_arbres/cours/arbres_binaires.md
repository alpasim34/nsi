# **Structure de données : les arbres binaires**

## **Mise en situation**

Un organisateur de tournoi de rugby recherche la meilleure solution pour afficher les potentiels quarts de final, demi-finales et finale :

Au départ nous avons 4 poules de 4 équipes. Les 4 équipes d'une poule s'affrontent dans un mini championnat (3 matchs par équipe). À l'issue de cette phase de poule, les 2 premières équipes de chaque poule sont qualifiées pour les quarts de finale.

Dans ce qui suit, on désigne les 2 qualifiés par poule par :
- Poule 1 => 1er Eq1 ; 2e Eq8
- Poule 2 => 1er Eq2 ; 2e Eq7
- Poule 3 => 1er Eq3 ; 2e Eq6
- Poule 4 => 1er Eq4 ; 2e Eq5

En quart de final on va avoir :
- quart de finale 1 => Eq1 contre Eq5
- quart de finale 2 => Eq2 contre Eq6
- quart de finale 3 => Eq3 contre Eq7
- quart de finale 4 => Eq4 contre Eq8

Pour les demi-finales on aura :
- demi-final 1 => vainqueur quart de finale 1 contre vainqueur quart de finale 3
- demi-final 2 => vainqueur quart de finale 2 contre vainqueur quart de finale 4

L'organisateur du tournoi affiche les informations ci-dessus le jour du tournoi. Malheureusement, la plupart des spectateurs se perdent quand ils cherchent à déterminer les potentielles demi-finales (et ne parlons pas de la finale !)

Pourtant, un simple graphique aurait grandement simplifié les choses :

![poules](./poules.png)

Les spectateurs peuvent alors recopier sur un bout de papier ce schéma et ensuite se livrer au jeu des pronostiques.

Nous avons ci-dessus ce que l'on appelle une structure en arbre. On peut aussi retrouver cette même structure dans un arbre généalogique :

![genealogie](./genealogie.png)

Dernier exemple, les systèmes de fichiers dans les systèmes de type UNIX ont aussi une structure en arbre (notion vue en première)

![fichiers](./fichiers.png)

Les arbres sont des types abstraits très utilisés en informatique. On les utilise notamment quand on a besoin d'une structure hiérarchique des données : dans l'exemple ci-dessus le fichier ```grub.cfg``` ne se trouve pas au même niveau que le fichier ```rapport.odt``` (le fichier ```grub.cfg``` se trouve "plus proche" de la racine / que le fichier ```rapport.odt```).

Un arbre est donc une structure représenté sous la forme d'une hiérarchie dont chaque élément est appelé *nœud*, le nœud initial étant appelé *racine* de l'arbre.

On distingue deux catégories d'éléments :

- les **feuilles** (ou nœuds externes), éléments ne possédant pas de fils dans l'arbre
- les **nœuds internes**, éléments possédant des fils (sous-branches)

Les arbres binaires sont des cas particuliers d'arbre : l'arbre du tournoi de rugby et l'arbre "père, mère..." sont des arbres binaires, en revanche, l'arbre représentant la structure du système de fichier n'est pas un arbre binaire. Dans un arbre binaire, on a au maximum 2 branches qui partent d'un élément (pour le système de fichiers, on a 7 branches qui partent de la racine : ce n'est donc pas un arbre binaire). Dans la suite nous allons uniquement travailler sur les arbres binaires.

## **Un peu de vocabulaire**

Soit l'arbre binaire suivant :

![exemple](./exemple.png)

- Chaque élément de l'arbre est appelé **noeud** (par exemple : A, B, C, D,...,P et Q sont des noeuds)
- Le noeud initial (A) est la **racine** de l'arbre
- On dira que les noeuds E et D sont les fils du noeud B. On dira que le noeud B est le père des noeuds E et D
- Dans un arbre binaire, un noeud possède **au plus** 2 fils
- Un noeud n'ayant aucun fils est appelé **feuille** (exemples : D, H, N, O, J, K, L, P et Q sont des feuilles)
- À partir d'un noeud (qui n'est pas une feuille), on peut définir un **sous-arbre gauche** et un **sous-arbre droite** (exemple : à partir de C on va trouver un sous-arbre gauche composé des noeuds F, J et K et un sous-arbre droit composé des noeuds G, L, M, P et Q)
- On appelle **arête** le segment qui relie 2 noeuds

## **Un peu de calcul**

Toujours sur l'arbre binaire vu plus haut :

- On appelle **taille** d'un arbre le nombre de noeuds présents dans cet arbre
- On appelle **profondeur** d'un noeud le nombre d'arêtes sur la branche qui le relie à la racine. La profondeur de la racine est nulle. Exemples : profondeur de B = 1 ; profondeur de I = 3 ; profondeur de P = 4
- On appelle **hauteur** d'un arbre la profondeur maximale des noeuds de l'arbre. Exemple : la profondeur de P = 4, c'est un des noeuds les plus profond, donc la hauteur de l'arbre est de 4

Il est aussi important de bien noter que l'on peut aussi voir les arbres comme des structures récursives : les fils d'un noeud sont des arbres (**sous-arbre gauche** et un **sous-arbre droite** dans le cas d'un arbre binaire), ces arbres sont eux mêmes constitués d'arbres.

## **Des formes particulières**

Un arbre binaire **strict** est un arbre dont tous les nœuds internes possèdent exactement deux fils. (Autrement dit, un arbre binaire strict est un arbre dont chaque nœud possède zéro ou 2 fils. L'arbre vide n'est pas strict.)

Un arbre binaire **complet** est un arbre dans lequel toutes les feuilles sont à la même profondeur. (Il s'agit d'un arbre dont tous les niveaux sont remplis.)

Un arbre binaire **parfait** est un arbre dans lequel tous les niveaux sont remplis à l'exception éventuelle du dernier, et dans ce cas les feuilles du dernier niveau sont alignées à gauche.

Un arbre binaire **équilibré** est un arbre dont les deux fils sont des arbres équilibrés dont les hauteurs diffèrent d'au plus une unité.  Ainsi, l'accès à n'importe lequel des nœuds est en moyenne minimisé.

## **Parcours d'arbres**

Parcourir un arbre consiste à visiter chacun des noeuds de l'arbre, à effectuer un traitement sur chacune des étiquettes des noeuds de l'arbre.

On distingue différents parcours selon l'ordre dans lequel les noeuds seront visités : des parcours en **profondeur** et en **largeur**.

## Parcours en profondeur

### **Trois parcours en profondeur**

Le principe des parcours en profondeur est de visiter récursivement les noeuds de l'arbre.

On distingue trois parcours, en fonction de comment l'on traite l'étiquette d'un noeud :

- avant la visite de ses fils gauche et droit
- après la visite de son fils gauche (et avant la visite de son fils droit)
- après la visite des ses fils gauche et droit

Pour désigner ces trois parcours en profondeur, on utilise respectivement les termes :

- préfixe
- infixe
- postfixe

## Parcours en largeur

Un parcours en largeur visite les noeuds d'un arbre niveau par niveau : le noeud racine de profondeur nulle, les noeuds de profondeur 1, puis les noeuds de profondeur 2, etc...

Pour une profondeur donnée, on visite les noeuds de gauche à droite.

Le principe est que lors de la visite du niveau de profondeur *i*, on mémorise dans une structure ad hoc les noeuds fils, qui sont donc des noeuds de profondeur *i+1* qui seront à visiter une fois la visite de la profondeur *i* terminée.

La structure dans laquelle mémoriser les noeuds fils doit permettre :
- d'ajouter un élément (un noeud) à la structure
- de récupérer un élément ; plus précisément de récupérer l'élément le plus anciennement inséré
- de tester que la structure est vide (fin de l'algorithme)

On reconnaît là l'interface du type de données abstrait file qui met en œuvre le principe FIFO : premier arrivé, premier sorti.

Pour la réalisation de notre parcours en largeur, les éléments de cette file sont des arbres : l'arbre à parcourir, puis l'arbre fils gauche de sa racine, puis l'arbre fils droit de sa racine, etc...

## **Les arbres binaires de recherche - ABR**

Un **arbre de recherche**, ABR, est un arbre binaire qui va être utilisé pour réaliser « efficacement » des opérations de recherche d'une valeur, mais aussi des opérations d'insertion et suppression de valeurs.

Les valeurs des étiquettes de l'arbre doivent donc appartenir à un ensemble ordonné.

Un ABR a pour tout noeud d'étiquette ```e``` :
- les étiquettes de tous les noeuds de son sous-arbre gauche sont inférieures ou égales à ```e```
- les étiquettes de tous les noeuds de son sous-arbre droit sont supérieures à ```e```.

## **Algorithmes à connaitre et à savoir expliquer**

<table>
<h3>Calculer la taille d'un arbre</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) un arbre binaire
return:
    (int) la taille de l'arbre
algorithme:
    si arbre est vide:
        return 0
    sinon si arbre est une feuille:
        return 1
    sinon:
        return 1 + taille(fils gauche) + taille(fils droit)
```

</td>
<td>

```python
def taille(arbre):
    if arbre.is_empty():
        return 0
    elif arbre.is_leaf():
        return 1
    else:
        return 1 + taille(arbre.get_left_subtree()) + taille(arbre.get_right_subtree())
```

</td>
</tr>
</table>

<table>
<h3>Calculer la profondeur d'un noeud</h3>
<tr>
<td>

```
parametres:
    (BinaryTree) une arbre binaire
    (int/str) la profondeur du noeud que l'on recherche
return:
    (int) la profondeur du noeud
algorithme:
    si arbre est vide:
        return 0
    sinon si racine est noeud:
        return 1
    sinon:
        return 1 + minimum(profondeur(fils gauche, noeud), profondeur(fils droit, noeud))
```

</td>
<td>

```python
# On supposera que le noeud que l'on cherche se trouve dans l'arbre
def profondeur(arbre, noeud):
    if arbre.is_empty():
        return 0
    elif arbre.get_data()==noeud:
        return 1
    else:
        return 1 + min(profondeur(arbre.get_left_subtree(), noeud), profondeur(arbre.get_right_subtree(), noeud))
```

</td>
</tr>
</table>

<table>
<h3>Calculer la hauteur d'un arbre</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) une arbre binaire
return:
    (int) la hauteur de l'arbre
algorithme:
    si arbre est vide:
        return 0
    sinon si arbre est une feuille:
        return 1
    sinon:
        return 1 + maximum(hauteur(fils gauche), hauteur(fils droit))
```

</td>
<td>

```python
def hauteur(arbre):
    if arbre.is_empty():
        return 0
    elif arbre.is_leaf():
        return 1
    else:
        return 1 + max(hauteur(arbre.get_left_subtree()), hauteur(arbre.get_right_subtree()))
```

</td>
</tr>
</table>

<table>
<h3>Parcourir un arbre avec un parcours en profondeur préfixe</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) un arbre binaire
return:
    (list) la liste du parcours en profondeur prefixe
algorithme:
    si arbre est une feuille:
        return [racine]
    sinon si pas de fils gauche:
        si pas de fils droit:
            return [racine]
        sinon:
            return [racine] + parcours_prefixe(fils droit)
    sinon:
        si pas de fils droit:
            return [racine] + parcours_prefixe(fils gauche)
        sinon:
            return [racine] + parcours_prefixe(fils gauche) + parcours_prefixe(fils droit)
```

</td>
<td>

```python
# Affiche le parcours :
def parcours_prefixe(arbre):
    if not arbre.is_empty():
        print(arbre.get_data(), end="")
        afficher_C(arbre.get_left_subtree())
        afficher_C(arbre.get_right_subtree())

# Recuperer le parcours dans une liste :
def parcours_prefixe(arbre):
    if arbre.is_leaf():
        return [arbre.get_data()]
    elif arbre.get_left_subtree().is_empty():
        if arbre.get_right_subtree().is_empty():
            return [arbre.get_data()]
        else:
            return [arbre.get_data()] + parcours_prefixe(arbre.get_right_subtree())
    else:
        if arbre.get_right_subtree().is_empty():
            return [arbre.get_data()] + parcours_prefixe(arbre.get_left_subtree())
        else:
            return [arbre.get_data()] + parcours_prefixe(arbre.get_left_subtree()) + parcours_prefixe(arbre.get_right_subtree())
```

</td>
</tr>
</table>

<table>
<h3>Parcourir un arbre avec un parcours en profondeur infixe</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) un arbre binaire
return:
    (list) la liste du parcours en profondeur infixe
algorithme:
    si arbre est une feuille:
        return [racine]
    sinon si pas de fils gauche:
        si pas de fils droit:
            return [racine]
        sinon:
            return [racine] + parcours_infixe(fils droit)
    sinon:
        si pas de fils droit:
            return parcours_infixe(fils gauche) + [racine]
        sinon:
            return parcours_infixe(fils gauche) + [racine] + parcours_infixe(fils droit)
```

</td>
<td>

```python
# Affiche le parcours :
def parcours_infixe(arbre):
    if not arbre.is_empty():
        afficher_A(arbre.get_left_subtree())
        print(arbre.get_data(), end="")
        afficher_A(arbre.get_right_subtree())

# Recuperer le parcours dans une liste :
def parcours_infixe(arbre):
    if arbre.is_leaf():
        return [arbre.get_data()]
    elif arbre.get_left_subtree().is_empty():
        if arbre.get_right_subtree().is_empty():
            return [arbre.get_data()]
        else:
            return [arbre.get_data()] + parcours_infixe(arbre.get_right_subtree())
    else:
        if arbre.get_right_subtree().is_empty():
            return parcours_infixe(arbre.get_left_subtree()) + [arbre.get_data()]
        else:
            return parcours_infixe(arbre.get_left_subtree()) + [arbre.get_data()] + parcours_infixe(arbre.get_right_subtree())
```

</td>
</tr>
</table>

<table>
<h3>Parcourir un arbre avec un parcours en profondeur postfixe</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) un arbre binaire
return:
    (list) la liste du parcours en profondeur postfixe
algorithme:
    si arbre est une feuille:
        return [racine]
    sinon si pas de fils gauche:
        si pas de fils droit:
            return [racine]
        sinon:
            return parcours_postfixe(fils droit) + [racine]
    sinon:
        si pas de fils droit:
            return parcours_postfixe(fils gauche) + [racine]
        sinon:
            return parcours_postfixe(fils gauche) + parcours_postfixe(fils droit) + [racine]
```

</td>
<td>

```python
# Affiche le parcours :
def parcours_postfixe(arbre):
    if not arbre.is_empty():
        afficher_B(arbre.get_left_subtree())
        afficher_B(arbre.get_right_subtree())
        print(arbre.get_data(), end="")

# Recuperer le parcours dans une liste :
def parcours_postfixe(arbre):
    if arbre.is_leaf():
        return [arbre.get_data()]
    elif arbre.get_left_subtree().is_empty():
        if arbre.get_right_subtree().is_empty():
            return [arbre.get_data()]
        else:
            return parcours_postfixe(arbre.get_right_subtree()) + [arbre.get_data()]
    else:
        if arbre.get_right_subtree().is_empty():
            return parcours_postfixe(arbre.get_left_subtree()) + [arbre.get_data()]
        else:
            return parcours_postfixe(arbre.get_left_subtree()) + parcours_postfixe(arbre.get_right_subtree()) + [arbre.get_data()]
```

</td>
</tr>
</table>

<table>
<h3>Parcourir un arbre avec un parcours en largeur</h3>
<tr>
<td>

```
parametre:
    (BinaryTree) un arbre binaire
return:
    (list) la liste du parcours en largeur de l'arbre
algorithme:
    si arbre est feuille:
        return [racine]
    sinon:
        res <- [racine]
        fil <- File()
        enfile fils gauche dans fil
        enfile fils droit dans fil
        Tant que fil n'est pas vide:
            fils <- defile fil
            si fils n'est pas une feuille:
                enfile fils gauche de fils dans fil
                enfile fils droit de fils dans fil
            ajoute à la fin de res racine de fils
        return res
```

</td>
<td>

```python
def parcours_largeur(arbre):
    if arbre.is_leaf():
        return [arbre.get_data()]
    else:
        res = [arbre.get_data()]
        fil = File()
        fil.enfile(arbre.get_left_subtree())
        fil.enfile(arbre.get_right_subtree())
        while not fil.est_vide():
            tmp = fil.defile()
            if not tmp.is_leaf():
                fil.enfile(tmp.get_left_subtree())
                fil.enfile(tmp.get_right_subtree())
            res += [tmp.get_data()]
        return res
```

</td>
</tr>
</table>

<table>
<h3>Rechercher une valeur dans arbre binaire de recherche</h3>
<tr>
<td>

```
parametres:
    (BinaryTree) un arbre binaire
    (int/str) la valeur de l'étiquette du noeud que l'on recherche
return:
    (BinaryTree) un arbre ayant pour racine la valeur que l'on souhaite
algorithme:
    si racine == valeur:
        return arbre
    sinon si valeur < racine ET fils gauche non vide:
        return recherche(fils gauche, valeur)
    sinon si valeur > racine ET fils droit non vide:
        return recherche(fils droit, valeur)
    sinon:
        return None
```

</td>
<td>

```python
def recherche(arbre, valeur):
    if arbre.get_data()==valeur:
        return arbre
    elif valeur<arbre.get_data() and not arbre.get_left_subtree().is_empty():
        return recherche(arbre.get_left_subtree(), valeur)
    elif valeur>arbre.get_data() and not arbre.get_right_subtree().is_empty():
        return recherche(arbre.get_right_subtree(), valeur)
    else:
        return None
```

</td>
</tr>
</table>

<table>
<h3>Insérer une valeur dans un arbre binaire de recherche</h3>
<tr>
<td>

```
parametres:
    (BinaryTree) un arbre binaire
    (int/str) valeur que l'on souhaite ajouter
return:
    (BinaryTree) un arbre binaire avec la valeur ajouter
algorithme:
    si arbre est vide:
        return nouvel_arbre(valeur, vide, vide)
    sinon si valeur <= racine:
        return nouvel_arbre(racine, insertion(fils gauche, valeur), fils droit)
    sinon:
        return nouvel_arbre(racine, fils gauche, insertion(fils droit, valeur))
```

</td>
<td>

```python
def insertion(arbre, valeur):
    if arbre.is_empty():
        return BinaryTree(valeur, empty, empty)
    elif valeur<=arbre.get_data():
        return BinaryTree(arbre.get_data(), insertion(arbre.get_left_subtree(), valeur), arbre.get_right_subtree())
    else:
        return BinaryTree(arbre.get_data(), arbre.get_left_subtree(), insertion(arbre.get_right_subtree(), valeur))
```

</td>
</tr>
</table>

## **Complexité de l'algorithme de recherche dans un arbre binaire de recherche équilibré**

La complexité en temps d'un algorithme est l'évaluation du nombre d'opérations élémentaires qu'il va effectuer pour obtenir le résultat attendu. Cette estimation ne pouvant être exacte et dépendant trop des conditions matérielles et logicielles d'exécution de l'algorithme, seule l'évolution de la complexité en fonction du nombre de données `n` à traiter donne une information pertinente.

![complexite_graph](complexite_graph.png)

Dans le cas de l'algorithme de recherche d'un élément dans un ABR équilibré, la complexité est la même que celle de la recherche dichotomique, c'est à dire $`\mathrm{O(ln(n))}`$. L'algorithme est donc très efficace.

Soit un ABR, de taille `n` et de hauteur `h`. 'n' étant le nombre total de noeuds, c'est aussi la quantité totale de données à traiter par l'algorithme de recherche. Lors de la recherche un changement de profondeur s'effectue grâce à une opération élémentaire, comme une comparaison. Ainsi, le nombre d'opérations élémentaires pour trouver un élément est égal aux nombre de noeuds parcourus, donc à sa profondeur. Dans le pire des cas, c'est-à-dire si l'élément recherché se trouve tout en bas de l'arbre, le nombre d'opérations à effectuer est donc égal à la hauteur `h` de l'arbre.

Ainsi pour un ABR la complexité de l'algorithme de recherche est égal à sa hauteur `h`. Celle-ci dépend de `n` et de la géométrie de l'arbre.

Lorsque l'arbre est dégénéré, dit aussi filiforme ou peigne, sa hauteur `h` est égale à sa taille `n`. La complexité dans le pire des cas est donc $`\mathrm{O(n)}`$.

Lorsque l'arbre est équilibré, la performance de l'algorithme est bien meilleure. En effet à chaque changement de profondeur, la taille du nombre de noeuds restant à analyser est divisée par deux.

Le tableau ci-dessous donne la taille d'un ABR équilibré en fonction de sa hauteur.

|profondeur h  |taille n  |
|:-:|:-:|
|1  |n = 1|
|2  |n = 3|
|3  |n = 7|
|4  |n = 15|
|...|...    |
|h  |n =  2<sup>h</sup> - 1|

La dernière relation du tableau permet d'obtenir la hauteur en fonction de la taille :

$`\mathrm{n = 2^h - 1}`$

$`\mathrm{\Longleftrightarrow 2^h = n + 1}`$

$`\mathrm{\Longleftrightarrow ln(2^h) = ln(n + 1)}`$

$`\mathrm{\Longleftrightarrow h . ln(2) = ln(n + 1)}`$

$`\mathrm{\Longleftrightarrow h = \dfrac {1} {ln(2)} . ln(n + 1) \approx 1.44  . ln(n + 1)}`$

Ainsi la complexité de l'algorithme de recherche dans un ABR est :

|ABR      |meilleur des cas|pire des cas      |
|:-------:|:--------------:|:----------------:|
|filiforme|O(1)            |O(n) = n          |
|équilibré|O(1)            |O(ln(n))          |
