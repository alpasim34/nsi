# Les arbres

## Les arbres en informatique

Les *arbres* sont des structures de données hiérarchiques, non-linéaire (contrairement aux tableaux indexés, listes chaînées, piles, et files) et  naturellement récursives, utilisées pour représenter des ensembles de données structurées hiérarchiquement telles que :

- les arborescences des systèmes de fichiers :

  [![système de fichiers](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/UnixFS1.svg/1920px-UnixFS1.svg.png)](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/UnixFS1.svg/1920px-UnixFS1.svg.png)

- les DOM en HTML :

[![DOM](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/DOM-model.svg/800px-DOM-model.svg.png)](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/DOM-model.svg/800px-DOM-model.svg.png)

- la modélisation d'univers en probabilité :

[![arbres de probabilité](https://upload.wikimedia.org/wikipedia/commons/e/e4/Arbre_proba.png)](https://upload.wikimedia.org/wikipedia/commons/e/e4/Arbre_proba.png)

- les arbres généalogiques dont on va emprunter le vocabulaire (`racine`, `parent`, `fils`,  ...)

  [![arbre généalogique](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Carl_Gustav_Bielke_antavla_001.jpg/443px-Carl_Gustav_Bielke_antavla_001.jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Carl_Gustav_Bielke_antavla_001.jpg/443px-Carl_Gustav_Bielke_antavla_001.jpg)

## Le vocabulaire des arbres.

### Terminologie

- un `nœud` est caractérisé par :
  - une `donnée` ou`étiquette` ou`élément`
  - un nombre fini de `fils`, qui sont eux-même des arbres.
- Un `arbre vide` ne contient aucun `noeud`. Pour assurer la cohérence de ces définitions, on considère que l'`arbre vide` n'est pas un `nœud`.
- une `arête` relie deux nœuds
- Chaque nœud, à l'exception de la `racine`, est relié à un autre nœud, son `père` ou `parent`, par exactement une arête entrante.
- Chaque `nœud` peut avoir une ou plusieurs `arêtes` sortantes le reliant à ses `fils`.
- la `racine` d'un arbre est le seul nœud sans `parent`.
- une `feuille` est un `nœud` sans `fils`.
- un `nœud interne` est un `nœud` qui n'est pas une `feuille`.
- un `sous-arbre` d'un `noeud` est l'ensemble constitué des `nœuds` descendant de ce `noeud` et de ses  `arêtes`.
- un `arbre binaire` est un 

### Mesures sur les arbres

- la `taille` d'un arbre est le nombre de nœuds de l'arbre.
- Un `chemin` est une liste de noeuds reliés par des arêtes.
- la `profondeur` d'un nœud est le nombre d'arêtes sur la branche qui le relie à la racine. La profondeur de la racine est nulle.
- la `hauteur` d'un arbre est la profondeur maximale de l'ensemble des nœuds de l'arbre. On conviendra conventionnellement que la hauteur de l'arbre vide est -1.
- l'`aridité d'un nœud` est le nombre de fils du nœud.
- l'`aridité d'un arbre` est le nombre maximal de fils des nœuds de l'arbre.



###  **Des formes particulières**

Un arbre **binaire** est un arbre d'arité 2, c'est à dire que tous ses noeuds ont au plus 2 fils 


Un arbre binaire **strict** est un arbre dont tous les nœuds internes possèdent exactement deux fils. (Autrement dit, un arbre binaire strict est un arbre dont chaque nœud possède zéro ou 2 fils. L'arbre vide n'est pas strict.)

Un arbre binaire **complet** est un arbre dans lequel toutes les feuilles sont à la même profondeur. (Il s'agit d'un arbre dont tous les niveaux sont remplis.)

Un arbre binaire **parfait** est un arbre dans lequel tous les niveaux sont remplis à l'exception éventuelle du dernier, et dans ce cas les feuilles du dernier niveau sont alignées à gauche.

Un arbre binaire **équilibré** est un arbre dont les deux fils sont des arbres équilibrés dont les hauteurs diffèrent d'au plus une unité.  Ainsi, l'accès à n'importe lequel des nœuds est en moyenne minimisé.

