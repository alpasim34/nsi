# Niveau 1 :

```python
avancer()
droite()
avancer()
gauche()
avancer()
droite()
for i in range(17):
	avancer()
ouvrir()
```


# Niveau 2 :

```python
for i in range(6):
	sauter()
	avancer()
avancer()
gauche()
avancer()
avancer()
for i in range(9):
	coup()
	avancer()
avancer()
avancer()
ouvrir()
```


# Niveau 3 :

```python
sauter_hauteur(4)
avancer()
sauter_hauteur(3)
for i in range(4):
	avancer()
	avancer()
	n = lire_nombre()
	avancer()
	sauter_hauteur(n)
avancer()
avancer()
```


# Niveau 4 :

```python
avancer()
for i in range(5):
	message = lire_chaine()
	if message == "gau":
		gauche()
	else:
		droite()
	avancer()
	avancer()
avancer()
ouvrir()
```


# Niveau 5 :

```python
avancer()
sauter_haut()
for i in range(15):
	h = mesurer_hauteur()
	if h == 2:
		sauter_haut()
	elif h == 1:
		sauter()
	else:
		avancer()
ouvrir()
```


# Niveau 6 :

```python
for i in range(6):
	sauter_hauteur(i)
	avancer()
ouvrir()
```


# Niveau 7 :

```python
for i in range(3, 11):
	for j in range(1, i):
		tirer(j)
	tourner()
```


# Niveau 8 :

```python
avancer()
while detecter_obstacle() == True:
	coup()
for i in range(3):
	avancer()
droite()
for i in range(5):
	avancer()
while detecter_obstacle() == True:
	coup()
	avancer()
ouvrir()
```
