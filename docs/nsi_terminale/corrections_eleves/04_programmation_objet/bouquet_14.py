#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Fleur:
    def __init__(self):
        self.couleur = 'blanc'
        self.taille = 20
        self.nbr_petales = 10
        self.nbr_feuilles = 0
        
    def affiche_caracteristiques(self):
        print('Caractéristiques de la fleur :')
        print('  - couleur :', self.couleur)
        print('  - taille :', self.taille)
        print('  - nombre de pétales :', self.nbr_petales)
        print('  - nombre de feuilles :', self.nbr_feuilles)

    def ajoute_petale(self):
        self.nbr_petales = self.nbr_petales + 1
    
    def enleve_petale(self):
        if self.nbr_petales > 0:
            self.nbr_petales = self.nbr_petales - 1

    def allonge(self, x):
        assert x >= 0, "le paramètre doit être positif ou nul"
        
        self.taille = self.taille + x
        
    def pousse(self):
        self.taille = self.taille * 1.1


## Déclaration des fonctions


## Programme principal

fleur_1 = Fleur()
fleur_2 = Fleur()
fleur_3 = Fleur()

fleur_2.couleur = 'rouge'
fleur_2.nbr_petales = 8
fleur_2.nbr_feuilles = 4
fleur_2.taille = 25

fleur_3.couleur = 'jaune'
fleur_3.nbr_petales = 16
fleur_3.nbr_feuilles = 8
fleur_3.taille = 12

bouquet = [fleur_1, fleur_2, fleur_3]

print(fleur_1.taille)
fleur_1.allonge(-5)
print(fleur_1.taille)