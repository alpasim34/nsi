#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Fleur:
    def __init__(self, couleur=['blanc'], taille=20, nbr_petales=10, nbr_feuilles=0):
        self.couleur = couleur
        self.taille = taille
        self.nbr_petales = nbr_petales
        self.nbr_feuilles = nbr_feuilles
        
    def affiche_caracteristiques(self):
        print('Caractéristiques de la fleur :')
        print('  - couleur :', self.couleur)
        print('  - taille :', self.taille)
        print('  - nombre de pétales :', self.nbr_petales)
        print('  - nombre de feuilles :', self.nbr_feuilles)

    def ajoute_petale(self):
        self.nbr_petales = self.nbr_petales + 1
    
    def enleve_petale(self):
        if self.nbr_petales > 0:
            self.nbr_petales = self.nbr_petales - 1

    def allonge(self, x):
        assert x >= 0, "le paramètre doit être positif ou nul"
        
        self.taille = self.taille + x
        
    def pousse(self):
        self.taille = self.taille * 1.1

    def esf_fanee(self):
#         #version 1
#         if self.nbr_petales == 0:
#             return True
#         else:
#             return False

        # version 2
        return self.nbr_petales == 0
    
    def croisement(self, autre_fleur):
        couleur = self.couleur + autre_fleur.couleur
        taille = (self.taille + autre_fleur.taille) / 2
        nbr_petales = (self.nbr_petales + autre_fleur.nbr_petales) // 2
        nbr_feuilles = (self.nbr_feuilles + autre_fleur.nbr_feuilles) // 2
        
        return Fleur(couleur, taille, nbr_petales, nbr_feuilles)

class Laboratoire_genetique:
    def __init__(self):
        pass

    def croisement(self, fleur_1, fleur_2):
        couleur = fleur_1.couleur + fleur_2.couleur
        taille = (fleur_1.taille + fleur_2.taille) / 2
        nbr_petales = (fleur_1.nbr_petales + fleur_2.nbr_petales) // 2
        nbr_feuilles = (fleur_1.nbr_feuilles + fleur_2.nbr_feuilles) // 2
        
        return Fleur(couleur, taille, nbr_petales, nbr_feuilles)


## Déclaration des fonctions


## Programme principal

labo = Laboratoire_genetique()

fleur_1 = Fleur()
fleur_2 = Fleur(['rouge'], 30, 7, 2)

fleur_3 = labo.croisement(fleur_1, fleur_2)

fleur_1.affiche_caracteristiques()
fleur_2.affiche_caracteristiques()
fleur_3.affiche_caracteristiques()