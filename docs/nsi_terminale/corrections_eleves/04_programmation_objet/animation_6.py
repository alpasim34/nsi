#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import tkinter
import time


## Déclaration des classes


## Déclaration des fonctions

def soleil(x, y):
    zone_graphique.create_oval(x, y, x + 100, y + 100, fill='yellow', outline='yellow')

def sol(y):
    zone_graphique.create_rectangle(0, y, 800 + 100, y + 50, fill='grey', outline='grey')

def oiseau(x, y):
    zone_graphique.create_line(x, y, x + 30, y + 20)
    zone_graphique.create_line(x + 30, y + 20, x + 60, y)

def voiture(x, y):
    zone_graphique.create_rectangle(x + 30, y, x + 120, y + 30, fill='blue', outline='blue')
    zone_graphique.create_rectangle(x, y + 30, x + 150, y + 60, fill='red', outline='red')
    zone_graphique.create_oval(x + 10, y + 50, x + 40, y + 80, fill='yellow', outline='yellow')
    zone_graphique.create_oval(x + 110, y + 50, x + 140, y + 80, fill='yellow', outline='yellow')

def animation():
    running = True
    x = 100
    delta_x = +30
    
    while running == True :
        zone_graphique.delete('all')

        soleil(20, 20)
        sol(550)
        oiseau(x, 200)
        voiture(100, 470)

        zone_graphique.update()
        
        x = x + delta_x
        
        if x < 100 or x > 700:
            delta_x = -1 * delta_x
        
        time.sleep(0.1)


## Programme principal

fenetre = tkinter.Tk()

fenetre.title('Animation')

zone_graphique = tkinter.Canvas(fenetre, width=800, height=600, bg='white')
zone_graphique.pack()

animation()

fenetre.mainloop()
