#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Fleur:
    def __init__(self):
        self.couleur = 'blanc'
        self.taille = 20
        self.nbr_petales = 10
        self.nbr_feuilles = 0

## Déclaration des fonctions


## Programme principal

fleur_1 = Fleur()
fleur_2 = Fleur()
fleur_3 = Fleur()