#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Voiture:
    '''
    Classe permettant de créer des objets de type Voiture.
    '''
    
    def __init__(self, moteur, couleur, puissance):
        self.moteur = moteur
        self.couleur = couleur
        self.puissance = puissance
    
    def caracteristiques(self):
        return {"moteur":self.moteur, "couleur":self.couleur, "puissance":self.puissance}

    def modifie_couleur(self, couleur):
        self.couleur = couleur
        
    def __del__(self):
        print('objet détruit')


## Déclaration des fonctions


## Programme principal

voiture_1 = Voiture("diesel", "rouge", 130)
voiture_2 = Voiture("essence", "jaune", 160)

print(voiture_1)
print(voiture_2)