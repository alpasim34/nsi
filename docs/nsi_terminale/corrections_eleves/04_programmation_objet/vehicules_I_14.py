#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Voiture:
    '''
    Classe permettant de créer des objets de type Voiture.
    '''
    
    def __init__(self, moteur, couleur, puissance, boite_vitesse, masse_vide, masse_PTAC):
        self.moteur = moteur
        self.couleur = couleur
        self.puissance = puissance
        self.nombre_portes = 5
        self.masse = masse_vide
        self.boite_vitesse = boite_vitesse
        self.masse_vide = masse_vide
        self.masse_PTAC = masse_PTAC
    
    def caracteristiques(self):
        return {"moteur":self.moteur, "couleur":self.couleur, "puissance":self.puissance, "boite_vitesse":self.boite_vitesse, "masse_vide":self.masse_vide, "masse_PTAC":self.masse_PTAC}

    def modifie_couleur(self, couleur):
        self.couleur = couleur

    def modifie_moteur(self, moteur):
        self.moteur = moteur

    def modifie_puissance(self, puissance):
        self.puissance = puissance
        
    def affiche_caracteristiques(self):
        dico = self.caracteristiques()
        
        print('Les caractéristiques du véhicule sont :')
#         for paire in dico.items():
#             print("  ", paire[0], ":", paire[1])
        
        # On peut aussi faire :
        for cle, valeur in dico.items():
            print("  ", cle, ":", valeur)       

    def charge(self, valeur):
        nouvelle_masse = self.masse + valeur
        if nouvelle_masse <= self.masse_PTAC:
            self.masse = nouvelle_masse
            return True
        else:
            return False

    def __del__(self):
        print('objet détruit')


## Déclaration des fonctions


## Programme principal

voiture_1 = Voiture("diesel", "rouge", 130, 'automatique', 700, 2000)

print(voiture_1.masse)
print(voiture_1.charge(1500))
print(voiture_1.masse)
