#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Fleur:
    def __init__(self):
        self.couleur = 'blanc'
        self.taille = 20
        self.nbr_petales = 10
        self.nbr_feuilles = 0

## Déclaration des fonctions


## Programme principal

fleur_1 = Fleur()
fleur_2 = Fleur()
fleur_3 = Fleur()

fleur_2.couleur = 'rouge'
fleur_2.nbr_petales = 8
fleur_2.nbr_feuilles = 4
fleur_2.taille = 25

fleur_3.couleur = 'jaune'
fleur_3.nbr_petales = 16
fleur_3.nbr_feuilles = 8
fleur_3.taille = 12

bouquet = [fleur_1, fleur_2, fleur_3]

print(bouquet)