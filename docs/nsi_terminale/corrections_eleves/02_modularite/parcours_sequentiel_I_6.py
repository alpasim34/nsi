#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import random


## Déclaration des constantes


## Déclaration des fonctions

def moyenne(tableau):
    '''
    Calcule la moyenne d'un tableau de valeurs.
    
    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''
    
    somme = 0
    n = len(tableau)
    for i in range(n):
        somme = somme + tableau[i]
    
    moyenne = somme / n
    
    return moyenne


## Fonction principale

def main():
#    resultat = moyenne([5, 7, 12, 1, 0, 8, 13, 20, 19])
#    print(resultat)
    
#    help(moyenne)
#    print(moyenne.__doc__)

    tableau = [random.randint(0, 100) for i in range(10)]
    
    print(tableau)


## Programme principal

if __name__ == '__main__':
    main()