#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import random


## Déclaration des constantes


## Déclaration des fonctions

def moyenne(tableau):
    '''
    Calcule la moyenne d'un tableau de valeurs.
    
    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''
    
    somme = 0
    n = len(tableau)
    for i in range(n):
        somme = somme + tableau[i]
    
    moyenne = somme / n
    
    return moyenne

def creation_tableau(n):
    '''
    Crée un tableau de n valeurs aléatoires.
    
    @param_entree : n, nombre entier
    @param_sortie : tableau, liste d'entiers
    '''

    tableau = [random.randint(0, 100) for i in range(n)]
    
    return tableau


## Fonction principale

def main():
    
    tableau = creation_tableau(200)
    print(tableau)


## Programme principal

if __name__ == '__main__':
    main()