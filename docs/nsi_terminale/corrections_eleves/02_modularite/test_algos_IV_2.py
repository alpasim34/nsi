#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import recherche
import time


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau = list(range(0, 1000))

print('Exécution des opérations.')
t1 = time.perf_counter()
# resultat = recherche.recherhe_une_occurrence(tableau, -1)
resultat = recherche.recherhe_une_occurrence(tableau, -1)
t2 = time.perf_counter()
print('Exécution terminée.')

print('Les opérations ont duré', t2 - t1, 'secondes.')

print(resultat)


# tableau = list(range(0, 1000))
# 
# print('Exécution des opérations.')
# t1 = time.perf_counter()
# # resultat = recherche.recherhe_une_occurrence(tableau, -1)
# for i in range(1000000):
#     resultat = recherche.recherche_dichotomique(tableau, -1)
# t2 = time.perf_counter()
# print('Exécution terminée.')
# 
# print('Les opérations ont duré', t2 - t1, 'secondes.')
# 
# print(resultat)
