#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import tris_factorises
import time


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau_a_trier = list(range(1000, 0, -1))
# print(tableau_a_trier)

print('Exécution des opérations.')
t1 = time.perf_counter()
tableau_trie = tris_factorises.tri_insertion(tableau_a_trier)
t2 = time.perf_counter()
print('Exécution terminée.')

print('Les opérations ont duré', t2 - t1, 'secondes.')
