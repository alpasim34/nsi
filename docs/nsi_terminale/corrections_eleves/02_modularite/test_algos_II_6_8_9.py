#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import parcours_sequentiel
import time


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau = parcours_sequentiel.creation_tableau(1000)
#print(tableau)

print('Exécution des opérations.')
t1 = time.perf_counter()
resultat = parcours_sequentiel.insere_element(tableau, 9999, 0)
t2 = time.perf_counter()
print('Exécution terminée.')

print('Les opérations ont duré', t2 - t1, 'secondes.')

#print(resultat)