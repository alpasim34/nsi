#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules


## Déclaration des constantes


## Déclaration des fonctions

def moyenne(tableau):
    '''
    Calcule la moyenne d'un tableau de valeurs.
    
    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''
    
    somme = 0
    n = len(tableau)
    for i in range(n):
        somme = somme + tableau[i]
    
    moyenne = somme / n
    
    return moyenne


## Programme principal