#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

import parcours_sequentiel
import tris
import time


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau = parcours_sequentiel.creation_tableau(10)
print(tableau)

tableau = tris.tri_selection(tableau)
print(tableau)

tableau = parcours_sequentiel.creation_tableau(10)
print(tableau)

tableau = tris.tri_insertion(tableau)
print(tableau)

#print('Exécution des opérations.')
#t1 = time.perf_counter()
#resultat = tris.tri_selection(tableau)
#t2 = time.perf_counter()
#print('Exécution terminée.')
#
#print('Les opérations ont duré', t2 - t1, 'secondes.')
#
##print(resultat)