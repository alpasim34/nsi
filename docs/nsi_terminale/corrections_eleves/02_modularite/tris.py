#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Module contenant des fonctions de tri de tableaux.

Auteur : Ramstein Stéphane
Date de création : 12/09/2022
Contact : stephane.ramstein@ac-lille.fr
'''


## Importation des modules


## Déclaration des constantes


## Déclaration des fonctions

def tri_selection(tableau):
    '''
    Tri par sélection d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    n = len(tableau)
    for i in range(n):
        position_plus_petit = i
        for j in range(i, n):
            if tableau[j] < tableau[position_plus_petit]:
                position_plus_petit = j
        temp = tableau[i]
        tableau[i] = tableau[position_plus_petit]
        tableau[position_plus_petit] = temp
    
    return tableau

def tri_insertion(tableau):
    '''
    Tri par insertion d'un tableau de valeurs.
    
    @param_entree : tableau non trié
    @param_sortie : tableau trié
    '''
    
    n = len(tableau)
    for i in range(1, n):
        element_a_inserer = tableau[i]
        j = i
        while j > 0 and tableau[j - 1] > element_a_inserer:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = element_a_inserer

    return tableau


## Fonction principale

def main():
    pass

## Programme principal

if __name__ == '__main__':
    main()
