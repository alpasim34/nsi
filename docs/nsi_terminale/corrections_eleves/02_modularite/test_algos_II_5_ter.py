#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Importation des modules

from parcours_sequentiel import creation_tableau, somme


## Déclaration des constantes


## Déclaration des fonctions


## Programme principal

tableau = creation_tableau(100)
print(tableau)

resultat = somme(tableau)
print(resultat)

resultat = moyenne(tableau)
print(resultat)

resultat = minimum(tableau)
print(resultat)

resultat = maximum(tableau)
print(resultat)

resultat = valeurs_extremales(tableau)
print(resultat)

resultat = recherche_une_occurrence(tableau, 10)
print(resultat)

resultat = recherche_toutes_occurrences(tableau, 10)
print(resultat)

resultat = insere_element(tableau, 9999, 10)
print(resultat)