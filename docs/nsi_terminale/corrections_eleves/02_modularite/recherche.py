#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Module contenant des fonctions de tri de tableaux.

Auteur : Ramstein Stéphane
Date de création : 12/09/2022
Contact : stephane.ramstein@ac-lille.fr
'''


## Importation des modules


## Déclaration des constantes


## Déclaration des fonctions

def recherhe_une_occurrence(tableau, element):
    '''
    Recherche un élément et retourne sa première position.

    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''
    n = len(tableau)
    i = 0
    position = None
    
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
            
    return position

def recherche_dichotomique(liste, element_recherche):
    '''
    Recherche un élément et retourne la première position trouvée.

    @param_entree : liste (triée)
    @param_entree : element_recherche
    @param_sortie : position
    '''

    n = len(liste)

    debut_sous_liste = 0
    fin_sous_liste = n - 1
    position = None
    trouve = False
    while trouve == False and debut_sous_liste <= fin_sous_liste:
        i_milieu = (debut_sous_liste + fin_sous_liste) // 2
        if liste[i_milieu] < element_recherche:
            debut_sous_liste = i_milieu + 1
        elif liste[i_milieu] > element_recherche:
            fin_sous_liste = i_milieu - 1
        elif liste[i_milieu] == element_recherche:
            trouve = True
            position = i_milieu

    return position


## Fonction principale

def main():
    pass

## Programme principal

if __name__ == '__main__':
    main()
