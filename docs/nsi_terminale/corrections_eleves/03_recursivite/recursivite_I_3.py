#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def fonction_5(n):
    print('execution de fonction_5(', n, ')')
    if n > 0:
        print(n)
        fonction_5(n - 1)
        print('retour de fonction_5(', n - 1, ') dans fonction_5(', n, ')')

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)


## Programme principal

fonction_5(4)