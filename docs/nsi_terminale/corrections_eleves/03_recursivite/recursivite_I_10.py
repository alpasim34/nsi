#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules

import time


## Déclaration des fonctions

def fonction_5(n):
    print('execution de fonction_5(', n, ')')
    if n > 0:
        print(n)
        fonction_5(n - 1)
        print('retour de fonction_5(', n - 1, ') dans fonction_5(', n, ')')

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)

def puissance_v1(x, n):
    if n == 0:
        return 1
    else:
        return x * puissance_v1(x, n - 1)

def puissance_v2(x, n):
    if n == 0:
        return 1
    elif n == 1:
        return x
    else:
        return x * puissance_v2(x, n - 1)

def puissance_v3(x, n):
    if n == 0:
        return 1
    elif n % 2 == 0:
        return (puissance_v3(x, n // 2)) ** 2
    else:
        return x * (puissance_v3(x, (n - 1) // 2)) ** 2

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def padovan(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    else:
        return padovan(n - 2) + padovan(n - 3)
    
def factorielle(n):
    if n == 1:
        return 1
    else:
        return n * factorielle(n - 1)

def compte_chiffres(n):
    if n <= 9:
        return 1
    else:
        return 1 + compte_chiffres(n // 10)


## Programme principal

resultat = compte_chiffres(9532)
print(resultat)
