#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def fonction_1(i):
    i = i + 1
    print(i)
    fonction_1(i)

def fonction_2(i):
    if i < 3:
        i = i + 1
        fonction_2(i)
    else:
        return None # ou pass

def fonction_3(i, n):
    if i < n:
        i = i + 1
        print(i)
        fonction_3(i, n)

def fonction_4(n):
    if n > 0:
        n = n - 1
        print(n)
        fonction_4(n)

def fonction_5(n):
    if n > 0:
        print(n)
        fonction_5(n - 1)

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)




## Programme principal

print(somme(3))