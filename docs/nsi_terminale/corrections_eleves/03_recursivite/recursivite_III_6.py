#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des fonctions

def hanoi(n, depart, arrivee, intermediaire):
    '''
    Fonction qui déplace n disques d'une tour de départ vers une tour d'arrivée
    en utilisant une tour intermédiaire.
    '''
    if n == 0:
        pass
    else:
        hanoi(n - 1, depart, intermediaire, arrivee)
        print(depart, 'vers', arrivee)
        hanoi(n - 1, intermediaire, arrivee, depart)


## Programme principal

hanoi(12, 'T1', 'T3', 'T2')