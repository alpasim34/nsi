# Classe de terminale


## Architectures matérielles, OS et réseaux - Circuits intégrés

Depuis l'invention du circuit intégré par Texas Instrument, le microprocesseur n'a cessé d'évoluer vers plus de puissance grâce à la miniaturisation de ses composants, tout en limitant la consommation d'énergie.

- Diapo cours : [pdf](./06_circuits_integres/cours.pdf) ou [odp](./06_circuits_integres/cours.odp)
- Fabrication d'un microprocesseur en images : [lien](https://www.tomshardware.fr/diapo-la-fabrication-dun-processeur-expliquee-en-images/)


## Architectures matérielles, OS et réseaux - Gestion des processus et des ressources

- Résumé de cours : [md](./07_processus_ressources/cours.md)
- Diapo cours : [pdf](./07_processus_ressources/cours.pdf) ou [odp](./07_processus_ressources/cours.odp)
- Fiche TP sur la gestion des processus : [pdf](./07_processus_ressources/TP_processus.pdf) ou [odt](./07_processus_ressources/TP_processus.odt)
- Fiche TP sur la gestion des ressources : [pdf](./07_processus_ressources/TP_fork_interblocage.pdf) ou [odt](./07_processus_ressources/TP_fork_interblocage.odt)


## Structures de données - Type abstrait LISTE et listes chaînées

Le type abstrait LISTE et ses implémentations sont abordées à travers structures de données tableaux statiques et listes chainées. Les types abstraits et les structures de données qui les implémentes sont spécifiées par leur interface.

- Diapo cours : [pdf](./05_listes_chainees_piles_files/cours.pdf) ou [odp](./05_listes_chainees_piles_files/cours.odp)
- Fiche TP : [pdf](./05_listes_chainees_piles_files/TP_Liste_Chainee.pdf)
- Fiche interface listes chaînées avec des tableaux : [odt](./05_listes_chainees_piles_files/Inteface_listes_chainees_tableaux.odt) ou [odt](./05_listes_chainees_piles_files/Inteface_listes_chainees_tableaux.pdf)

 <!-- ou [odt](./05_listes_chainees_piles_files/TP_liste_chainees.odt) -->


## Architectures matérielles, OS et réseaux - Protocoles de routage

Internet est l'assemblage de réseaux autonomes. Après leur présentation et un rappel des notions élémentaires, le cours étudie les deux protocoles permettant d'inscrire dans les tables de routage les chemins que doivent emprunter les paquets de données dans ce type de réseau : **R**outing **I**nformation **P**rotocol et **O**pen **S**hortest **P**ath **F**irst.

- Diapo de cours : [pdf](./12_protocoles_reseaux/cours.pdf) ou [odp](./12_protocoles_reseaux/cours.odp)
- Algorithme de Dijkstra, application sur le graphe : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.odp)
- Algorithme de Dijkstra, application en tableau : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.odp)
- Fiche TD sur les protocole RIP et OSPF : [pdf](./12_protocoles_reseaux/TD_protocoles_reseaux.pdf) ou [odt](./12_protocoles_reseaux/TD_protocoles_reseaux.odt)
- Tables de routage vierges pour le TD : [ods](./12_protocoles_reseaux/Tables_routage_vierges.ods)
- Quelques topologies : [pdf](./12_protocoles_reseaux/topo_reseaux.pdf) ou [odp](./12_protocoles_reseaux/topo_reseaux.odp)
- TP illustrant le protocole RIP en Python : [pdf](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.pdf) ou [odt](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.odt)

- Révisions de première : [pdf](../nsi_premiere/07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.pdf) ou [odp](../nsi_premiere/07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.odp)
    * Vidéo sur l'histoire d'internet : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/0_Historique_et_Principe.mp4)
    * Vidéo sur le fonctionnement d'internet : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/1_Internet_Comment_ca_marche.mp4)
    * Vidéo sur le protocole TCP-IP : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/3_MOOC_SNT_Internet_Protocole_TCP_IP.mp4)
    * Fiche sur les protocoles IP et TCP : [pdf](../nsi_premiere/07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf) ou [odt](../nsi_premiere/07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.odt)


## Structures de données - Type abstrait PILES et FILES

Il existe de nombreuses structures de données différentes. Choisir la plus adaptée permettra de traiter les données de manières plus simple, efficace et rapide.

On retrouve dans les piles une partie des propriétés vues sur les  listes. Dans les piles, il est uniquement possible de manipuler le  dernier élément introduit dans la pile. On prend souvent l'analogie avec une pile d'assiettes : dans une pile d'assiettes la seule assiette directement accessible et la dernière assiette qui a été déposée sur  la pile. 	

Comme les piles, les files ont des points communs avec les listes.  Différences majeures : dans une file on ajoute des éléments à une  extrémité de la file et on supprime des éléments à l'autre extrémité. On prend souvent l'analogie de la file d'attente devant un magasin  pour décrire une file de données. 	

- Diapo cours : [pdf](./05_listes_chainees_piles_files/diapo_piles_files.pdf) ou [odp](./05_listes_chainees_piles_files/diapo_piles_files.odp)
- Résumé de cours : [md](./05_listes_chainees_piles_files/cours_piles_files.md)
- Fiche exercices sur les piles et les files : [pdf](./05_listes_chainees_piles_files/Exercices_piles_files.pdf) ou [md](./05_listes_chainees_piles_files/Exercices_piles_files.md)
- Fiche TP sur les piles : [pdf](./05_listes_chainees_piles_files/Implémentation_des_piles.pdf) ou [md](./05_listes_chainees_piles_files/Implémentation_des_piles.md)
<!-- - Fiche TP sur les files : [pdf](./05_listes_chainees_piles_files/Implémentation_des_files.pdf) ou [md](./05_listes_chainees_piles_files/Implémentation_des_files.md) -->
- Fichiers Python pour les TP : [zip](./05_listes_chainees_piles_files/fichiers_python.zip)


## Projet de Noël

Réaliser un ordonnanceur en Python : [md](../projets/ordonnanceur/ordonnanceur.md)


## Révisions - Algorithmes gloutons et des plus proches voisins

- Algorithme glouton - Résumé de cours : [md](./13_techniques_programmation/revisions_glouton/cours.md)
- Algorithme glouton - Diapo de cours : [pdf](./13_techniques_programmation/revisions_glouton/diapo.pdf) ou [odp](./13_techniques_programmation/revisions_glouton/diapo.odp)
- Algorithme glouton - Fiche TP : [pdf](./13_techniques_programmation/revisions_glouton/TP_Le_probleme_des_pieces_de_monnaie.pdf) ou [md](./13_techniques_programmation/revisions_glouton/TP_Le_probleme_des_pieces_de_monnaie.md)
    - Fichier Python pour le TP : [zip](./13_techniques_programmation/revisions_glouton/monnaie_eleve.zip)
<br>

- Algorithme knn - Résumé de cours : [md](./13_techniques_programmation/revisions_knn/cours.md)
- Algorithme knn - Diapo de cours : [pdf](./13_techniques_programmation/revisions_knn/diapo.pdf) ou [odp](./13_techniques_programmation/revisions_knn/diapo.odp)
- Algorithme knn - Fiche TP : [pdf](./13_techniques_programmation/revisions_knn/TP_KNN.pdf) ou [md](./13_techniques_programmation/revisions_knn/TP_KNN.md)
    - Fichier Python pour le TP : [zip](./13_techniques_programmation/revisions_knn/vetements_eleve.zip)


## Algorithmique - Diviser pour régner

La méthode diviser pour régner est une méthode algorithmique qui consiste à décomposer un problème complexe en problèmes simples, résoudre les problèmes simples et combiner les solutions obtenues pour obtenir la solution du problème initial. La méthode fournit en général des algorithmes de complexité faible ce qui en fait son intérêt. L'algorithme d'Euclide, 300 av JC, pour calculer le plus grand commun diviseur de deux nombres peut être vu comme un algorithme diviser pour régner. Son introduction en informatique se fait via le tri radix en 1929, décrit pour les machines IBM à cartes à trou.

- Cours : [pdf](./13_techniques_programmation/cours_Diviser_regner.pdf) ou [odt](./13_techniques_programmation/cours_Diviser_regner.odt)
- Fiche TP : [pdf](./13_techniques_programmation/TP_Diviser_regner.pdf) ou [odt](./13_techniques_programmation/TP_Diviser_regner.odt)
    - Fichiers images pour le TP : [manchot](./13_techniques_programmation/manchot.png) et [lapins](./13_techniques_programmation/lapins.png)


## Structures de données et algorithmique - Arbres binaires, arbres binaires de recherche, autres structures arborescentes

- Diapo cours : [pdf](./08_arbres/Diapo_SR.pdf) ou [odp](./08_arbres/Diapo_SR.odp)
- Fiche exos 1 : [md](./08_arbres/exos_SR.md) ou [pdf](./08_arbres/exos_SR.pdf)
- Fiche tp 1 sur la construction d'arbre binaires : [pdf](./08_arbres/tp_arbres_binaires.pdf) ou [odt](./08_arbres/tp_arbres_binaires.odt)
- Fiche tp-cours sur les fonctions usuelles des arbres binaires : [pdf](./08_arbres/tp_fonctions_arbres.pdf) ou [odt](./08_arbres/tp_fonctions_arbres.odt)
- Fiche tp-cours sur les arbres binaires de recherche : [pdf](./08_arbres/tp_arbres_binaires_recherche.pdf) ou [odt](./08_arbres/tp_arbres_binaires_recherche.odt)
- Projet autour des arbres binaires proposé par Mieszczak Christophe : [md](https://framagit.org/tofmzk/informatique_git/-/blob/master/terminale_nsi/projets/le_hibou/le_hibou.md)
    - Aide pour le projet : [pdf](../projets/le_hibou/aide_hibou.pdf) ou [odt](../projets/le_hibou/aide_hibou.odt)
    - Classe Node avec affichage graphviz: [py](../projets/le_hibou/node_graphviz.py)


<!-- - Résumé de cours : [md](./08_arbres/cours/arbres_binaires.md) -->
<!-- - Fiche TP 1 sur le parcours en profondeur : [md](./08_arbres/tp/partie1/parcours_profondeur.md)
- Fiche TP 2 sur le parcours en largeur : [md](./08_arbres/tp/partie2/parcours_largeur.md)
- Fiche TP 3 sur les arbres binaires de recherche : [md](./08_arbres/tp/partie3/ABR.md)
- Module à utiliser pour le TP : [zip](./08_arbres/tp/arbres_binary_tree.zip)
- Fiche exercices 1 : [pdf](./08_arbres/exos/exos.pdf) ou [md](./08_arbres/exos/exos.md)
- Fiche exercices 2 : [pdf](./08_arbres/exos/exo2.pdf) ou [md](./08_arbres/exos/exo2.md) -->


## Langages et programmation - Paradigmes et programmation fonctionnelle, impérative, objet

Tout comme le paradigme impératif découle naturellement de l'approche du calcul par Alan Turing (machine de Turing), le paradigme fonctionnel découle naturellement de l'approche du calcul par Alonzo Churh (&lambda;-calcul). Les principaux paradigmes, du plus déclaratif au plus impératif sont présentés avant d'introduire les principes fondamentaux de la programmation fonctionnelle.

- Diapo de cours : [pdf](./11_programmation_fonctionnelle/diapo_cours.pdf) ou [odp](./11_programmation_fonctionnelle/diapo_cours.odp)
- Fiche TD introduisant les principes fondamentaux : [pdf](./11_programmation_fonctionnelle/TP_prog_fonctionnelle.pdf) ou [odt](./11_programmation_fonctionnelle/TP_prog_fonctionnelle.odt)
- Base du programme turtle utilisé dans le TD : [zip](./11_programmation_fonctionnelle/turtle_fonctionnelle.zip)


## Structures de données et algorithmique - Graphes et parcours en profondeur et en largeur

- Fiche TD-cours : [pdf](./14_graphes/Cours_TD_Graphe.pdf) ou [odt](./14_graphes/Cours_TD_Graphe.odt)
- Fiche TP sur l'implémentation des graphes avec des listes : [md](./14_graphes/implementation_liste.md)
    - fichier python pour le TP : [py](./14_graphes/module_graphe_liste.py)
- Fiche TP l'implémentation des graphes avec des matrices : [md](./14_graphes/implementation_mat.md)
    - fichier python pour le TP : [py](./14_graphes/module_graphe_mat.py)
- Fiche TD algorithmes sur les graphes : [pdf](./14_graphes/Cours-TD_AlgoGraphe.pdf) ou [odt](./14_graphes/Cours-TD_AlgoGraphe.odt)

<!-- - Résumé de cours : [pdf](./14_graphes/cours/graph.pdf) ou [md](./14_graphes/cours/graph.md)
- Diapo de cours : [pdf](./14_graphes/diapo.pdf) ou [odp](./14_graphes/diapo.odp)
- Fiche TD d'applications directes du cours : [pdf](./14_graphes/td/td.pdf) ou [md](./14_graphes/td/td.md)
- Fiche TP présentant une classe pour travailler sur les graphes : [pdf](./14_graphes/tp/tp.pdf) ou [md](./14_graphes/tp/tp.md)
- Fiche TD sur une mise en situation : [pdf](./14_graphes/td2/td.pdf) ou [md](./14_graphes/td2/td.md)
- Fiche TP sur la multiplication de matrices : [pdf](./14_graphes/tp2/corriger.pdf) ou [md](./14_graphes/tp2/corriger.md) -->


## Algorithmique - Diviser pour régner et Programmation dynamique

- Diapo de cours : [pdf](./13_techniques_programmation/cours.pdf) ou [odp](./13_techniques_programmation/cours.odp)
- Fiche TD sur les techniques de programmation : [pdf](./13_techniques_programmation/tp_techniques_programmation.pdf) ou [odt](./13_techniques_programmation/tp_techniques_programmation.odt)
- Illustration de la recherche dichotomique récursive : [pdf](./13_techniques_programmation/SR_08_dichotomie_recursive.pdf) ou [odp](./13_techniques_programmation/SR_08_dichotomie_recursive.odp)
- Illustration de la recherche dichotomique non récursive : [pdf](../nsi_premiere/10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.pdf) ou [odp](../nsi_premiere/10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.odp)
- Illustration de la fusion récursive : [pdf](./13_techniques_programmation/SR_05_fusion_recursive.pdf) ou [odp](./13_techniques_programmation/SR_05_fusion_recursive.odp)
- Illustration de la fusion non récursive : [pdf](./13_techniques_programmation/SR_04_fusion_non_recursive.pdf) ou [odp](./13_techniques_programmation/SR_04_fusion_non_recursive.odp)
- Illustration du tri fusion : [pdf](./13_techniques_programmation/SR_06_tri_fusion.pdf) ou [odp](./13_techniques_programmation/SR_06_tri_fusion.odp)


## Architectures matérielles, OS et réseaux - Sécurisation des communications

Depuis la seconde guerre mondiale et l'avènement de l'informatique moderne, la cryptologie est passée du statut d'art utilisé essentiellement par les militaires au statut de science utilisée par tous. Elle regroupe la cryptographie, élaborant et caractérisant les systèmes de chiffrage/déchiffrage des données et la cryptanalyse, tentant de casser les systèmes avec un nombre d'information limité.

- Cours à compléter : [md](./18_cryptographie/cours_marette/Sécurisation_des_communication_a_trou.md)
- TP sur le cryptage symétrique : [md](./18_cryptographie/cours_marette/Les_chiffrements_symétriques.md)
    - Code python à compléter : [py](./18_cryptographie/cours_marette/codage_symetrique.py)

<!-- - Diapo de cours, généralités et cryptage symétrique : [pdf](./18_cryptographie/cours_cryptographie.pdf) ou [odp](./18_cryptographie/cours_cryptographie.odp)
- Fiche cours-TD, cryptage asymétrique : [pdf](./18_cryptographie/SR_08_RSA.pdf) ou [odt](./18_cryptographie/SR_08_RSA.odt) -->


## Algorithmique - Recherche textuelle

La recherche textuelle consiste en la recherche des occurrences d’une suite de caractères, appelée le motif, dans une autre suite de caractères plus longue, appelée le texte. Les applications sont nombreuses allant de la simple recherche d’un mot dans un document, de mots clés dans une multitude de pages web, jusqu’à la recherche de l’enchaînements des bases nucléiques dans les molécules d’ADN.

- Diapo de cours, exemple d'application sur l'ADN : [pdf](./19_recherche_textuelle/diapo_cours.pdf) ou [odp](./19_recherche_textuelle/diapo_cours.odp)

<!-- - Diapo de cours, exemple d'application sur l'ADN : [pdf](./19_recherche_textuelle/algorithme_Boyer_Moore.pdf) ou [odp](./19_recherche_textuelle/algorithme_Boyer_Moore.odp)
- Fiche cours-TD, force brute et Boyer-Moore : [pdf](./19_recherche_textuelle/recherche_textuelle.pdf) ou [odt](./19_recherche_textuelle/recherche_textuelle.odt) -->


## Langages et programmation - Calculabilité, programme en tant que donnée

La théorie de la calculabilité définit ce que sont un nombre et une fonction calculable par un algorithme, tente de les recenser et d'appliquer son formalisme à d'autres domaines des mathématiques. Née dans les années 1930, elle a posé les bases des paradigmes de programmation fonctionnelle et impérative et du fonctionnement de la machine moderne.

- Diapo de cours : [pdf](./17_calculabilite/cours_diapo.pdf) ou [odp](./17_calculabilite/cours_diapo.odp)
- Fiche TD : [pdf](./17_calculabilite/TD_calculabilite.pdf) ou [odt](./17_calculabilite/TD_calculabilite.odt)
- Exemples d'algorithmes pour la machine de Turing : [pdf](./17_calculabilite/Machine_de_Turing_V2.pdf) ou [odp](./17_calculabilite/Machine_de_Turing_V2.odp)
- Machine de Turing en papier : [pdf](./17_calculabilite/Machine_Turing_papier.pdf) ou [odp](./17_calculabilite/Machine_Turing_papier.odp)


!!! note "Ont participé à l'élaboration de ces ressources :"
    **Marette Amélie**, étudiante master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 1ère année puis 2ème année, à l'Université de Lille.<br />
    **Zagacki Paul**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.<br />
    **Decoster Timothée**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.<br />
    **Ikli Youness**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.<br />
    **De Wancker Randy**, élève **N**umérique et **S**ciences **I**nfomatiques, terminale, lycée Raymond Queneau de Villeneuve d'Ascq.


<!-- ## Projets de Noël

Proposition de cinq projets à commencer pendant les vacances de Noël et développer en groupe à la maison avec des points en classe. Evaluation régulière du travail effectué. Compte rendu en Février par mail. Oraux en Mars.

- ASCII Art
- Simulateur de processeur
- Gestion des repas d’un restaurant scolaire
- Automate cellulaire
- Planète WA-TOR

- Diapo présentant les projets : [pdf](./10_projets_de_noel/projets_de_noel.pdf) ou [odp](./10_projets_de_noel/projets_de_noel.odp)

Déroulement des oraux:

- En groupe : 5 min x nombre d'élèves du groupe
- Une introduction où tous les élèves parlent
- Puis chaque élève développe la partie qu'il a travaillé : objectifs, réalisation, problèmes rencontrés, ...
- Conclusion et perspectives où tous les élèves parlent

Proposition de réalisations simples ou de compléments, en classe, après passage à l'oral des élèves du groupe correspondant :

- Jeu de la vie : [pdf](./10_projets_de_noel/Jeu_de_la_vie.pdf) ou [odt](./10_projets_de_noel/Jeu_de_la_vie.odt)
- Planète Wator : [pdf](./10_projets_de_noel/Wator.pdf) ou [odt](./10_projets_de_noel/Wator.odt)
  - Fichier python à comprendre et à compléter : [py](./10_projets_de_noel/wator_eleves.py)
- Kantoche 2000 : [pdf](./10_projets_de_noel/kantoche_2000.pdf) ou [odt](./10_projets_de_noel/kantoche_2000.odt)
  - Fichier compressé contenant le site web dynamique : [zip](./10_projets_de_noel/restauration_scolaire.zip)
- ASCII Art : [pdf](./10_projets_de_noel/ascii_art.pdf) ou [odt](./10_projets_de_noel/ascii_art.odt) -->


## Projet guidé en classe : jeu du serpent

Le but de ce type de projet guidé est de réinvestir les apprentissages tout en donnant aux élèves des éléments pour construire un projet à partir de la page blanche.

Le travail a réaliser est le jeu du serpent. Le serpent se déplace sur l'écran à l'aide des quatre touches de direction. Lorsqu'il mange une pomme il s'allonge et accélère. Le but est de manger toutes les pommes sans que le serpent se morde la queue.

[sur Wikipedia en anglais](https://en.wikipedia.org/wiki/Snake_(video_game_genre))

[sur Wikipedia en français](https://fr.wikipedia.org/wiki/Snake_(genre_de_jeu_vidéo))

Captures d'écran du jeu à réaliser : [png](./15_jeu_du_serpent/jeu_serpent_1.png) et [png](./15_jeu_du_serpent/jeu_serpent_2.png)

- Réflexion individuelle sur papier pour dégager les éléments principaux et une stratégie de programmation
- Brainstorming avec toute la classe à l'aide d'une carte mentale : [pdf](./15_jeu_du_serpent/Projet_jeu_du_serpent.pdf)
- Début de stratégie de programmation adoptée :
    * Le jeu, le serpent et une pomme sont des objets indépendants.
    * 1\. Commencer par coder la classe `Jeu()` en construisant la fenêtre et la zone graphique.
    * 2\. Coder la classe `Pomme()` et créer une liste d'objets `Pomme()` à des position aléatoires. L'afficher.
    * 3\. Coder la classe `Serpent()` et l'afficher.
    * 4\. Faire le point et organiser le traitement des autres éléments du jeu.


## Enjeux environnementaux de l'informatique

Les technologies de l’information et de la communication (TIC) ont pris une place centrale dans nos vies. Elles ont fondamentalement transformé notre façon de vivre et nous avons tendance à penser que ces technologies sont immatérielles. Les problématiques exposées sont :

- Quel est l'impact du numérique sur l’environnement ?
- Comment utiliser écologiquement nos appareils numériques ?

- Diapo de cours : [pdf](./16_enjeux_environnementaux/diapo-enjeux_environnementaux.pdf) ou [odp](./16_enjeux_environnementaux/diapo-enjeux_environnementaux.odp)
- Consignes pour l'activité 1 : [md](./16_enjeux_environnementaux/activite_1.md)
- Recommandations : [pdf](./16_enjeux_environnementaux/diapo-enjeux_environnementaux_recommandations.pdf) ou [odp](./16_enjeux_environnementaux/diapo-enjeux_environnementaux_recommandations.odp)

!!! note "Documents rédigés par :"
    **Ikli Youness**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.
