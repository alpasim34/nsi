import sqlite3

# connection à la base de données 
connection = sqlite3.connect('ma_base.db')

# création du curseur permettant d'entrer une requête SQL
# et de récupérer les résultats
curseur = connection.cursor()

# la méthode execute() permet d'exécuter des requêtes SQL
# et la méthode fetchall() permet d'en récupérer le résultat
# sous la forme d'une liste
curseur.execute('SELECT * FROM ma_table')
resultat = curseur.fetchall()

# la méthode commit() appliquée à la connection
# permet de mettre à jour la base après une modification
curseur.execute('INSERT INTO ... VALUES ...')
connection.commit()

# fermer la base de données en fin d'utilisation
connection.close()

