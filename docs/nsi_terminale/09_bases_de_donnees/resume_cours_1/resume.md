# Bases de données relationnelles et systèmes de Gestion de bases de données 

## Introduction
Depuis toujours l'homme cherche à traiter et stocker des données.

Il a commencé à le faire efficacement avec la mécanographie.

La mécanographie est utilisée depuis 1645 avec les machines à compteurs et en 1844 elle a vu apparaître la saisie par clavier.

La mécanographie est un système de stockage de données à l'aide de carte perforé.

Pour son utilisation plusieurs machines étaient nécessaires : la poinçonneuse, trieuse, interclasseuse, traductrice et la reproductrice.

Ce principe est utilisé pour la première à grande échelle en 1890 pour le recensement de la population américaine.

Avec l'arrivée de l'informatique moderne dans les années 1960 le système de stockage sur carte perforée est peu à peu abandonné pour un stockage sur bande magnétique puis sur disque dur, inventé par IBM. L'accès aux données se fait enfin directement.

En 1960 c'est l'apparition des bases de données dans le cadre de la mission Apollo qui a pour but de stocker les données en rapport avec le programme spatial.

L'électronique apporte un champ de possibilités immense que n'avait pas la mécanographie.

## I/Les Systèmes de Gestion de Bases de Données
Un Système de Gestion de Bases de Données (SGBD) est un outil informatique permettant la sauvegarde, l'interrogation, la recherche et la mise en forme de données.

Un SGBD est donc un ensemble de logiciels systèmes qui permettent aux programmateurs d'insérer, de modifier et de rechercher des données spécifiques dans une grande masse d'information partagée par plusieurs utilisateurs.

Ces informations sont généralement enregistrées sur des disques magnétiques (par exemple, des serveurs).

En plus des fonctions primaires citées dans la définition précédente, un SGBD :

- assure le partage des données

- vérifie qu'une opération s'effectue entièrement ou pas du tout : c'est ce que l'on nomme l'atomicité

- protège les données contre tout incident (détérioration, suppression accidentelle, ...)

- optimise les performances (temps de recherche minimisé par exemple)

Les SGBD permettent la description des données (définition des types par des noms, des formats, ...) de manière séparée de leur utilisation (mise à jour et recherche).

Ils permettent aussi de retrouver les caractéristiques d'un type de données à partir de son nom (par exemple, comment est décrit un produit).

Il existe cinq modèles de SGBD :

* le modèle hiérarchique : les données sont classées hiérarchiquement, selon une arborescence descendante (arbre). Ce modèle utilise des pointeurs entre les différents enregistrements. Il s'agit du premier modèle de SGBD.

![le modèle hierarchique](hierarchique.png)

* le modèle réseau : ce modèle ressemble au modèle hiérarchique à ceci près qu'il n'est plus nécessairement descendant.

![le moodèle réseau](reseau.png)

* Le modèle relationnel (SGBDR) : les données sont enregistrées dans des tableaux à deux dimensions (lignes et colonnes)

* le modèle déductif : similaire au modèle relationnel , mais la manipulation des tables se fait différemment

* le modèle objet (SGBDO) : les données sont stockées sous forme de classes

Depuis la fin des années 1990, le modèle relationnel est le plus répandu. Environ trois quarts des bases de données utilisent ce modèle.

Le modèle relationnel a été imaginé dans les années 1970 par l'informaticien britannique Edgar Franck Codd. Avant cela, les modèles de bases de données ne permettaient pas de décrire de façon satisfaisante les relations entre deux données à l'aide de pointeurs logiques.

## II/Bases de données non relationnelles, bases de données relationnelles : quelle différence ?

Une base de données contient un ensemble d'informations qui sont stockées, accessibles et gérées à d'aide d'un système de gestion de base de données (SGBD). Parmi les différents types de bases de données, il existe la base de données relationnelle, et la base de données non relationnelle.

Alors que la première stocke les données dans des tables, la deuxième les stocke au format clé-valeur, dans des documents, en colonne, en graphique ou autres.

Une base de données relationnelle relie les informations entre elles en stockant dans des tables en relations, qui peuvent être accessibles et reconstruites de différentes manières et qui sont elles-mêmes composées de lignes et de colonnes. Le langage de requête structuré (SQL) permet d'interroger les données de façon interactive et ainsi de les collecter dans le cadre de rapports.

## III/Le modèle de base de données relationnelle
Une relation ressemble à une table avec des valeurs.

Une **relation** contient un ensemble de lignes.

Les lignes d'une relation s'appellent des **n-uplets**.

Un n-uplet est un ensemble ordonné de valeurs.

Par exemple, une ligne dans la relation ELEVE est un 6-uplets et contient donc six valeurs.

Chaque colonne d'une relation a une entête qui s'appelle **l'attribut**.

Exemple la relation ELEVE :

![description](description.png)

Le **schéma** (ou description) d'une relation et noté R(A1, A2, ... , An) où:

- R est le nom de la relation

- A1, A2, ..., An sont les attributs de la relation

**Exemple**: ELEVE (nom, prénom, NIP, adresse, téléphone, age)

Chaque attribut a un ensemble de valeurs valides. Cet ensemble de valeurs est appelé un **domaine**. 

Un domaine à une signification. Chaque domaine à un type de données ou un format.

Les noms d'attributs sont en général suffisamment explicite pour indiquer le rôle joué par un domaine dans une relation.

Ils permettent d'indiquer la signification des valeurs qui correspondent à cet attribut

exemple: le domaine Date de format JJ/MM/AAAA peut servir à 2 attributs Date_facturation et Date_paiement.

**Contrainte de Domaine** : seule les valeurs présentent dans le domaine sont utilisables.

Chaque ligne contient une ou plusieurs valeurs qui identifie(nt) cette ligne sans ambiguïté par rapport aux autres. Ces valeurs sont appelées **clés**.

Dans la table ELEVE la clé est NIP.

Autre exemple : VOITURE (immatriculation, numéro_produit, marque, modèle, année)

La table voiture possède 2 clés, immatriculation et numéro de produit. On les appelle **clés candidates**.
Une de ces clés candidates est sélectionnée comme **clé primaire**.

La clé primaire est soulignée dans le schéma.

La valeur de la clé primaire permet d'identifier de manière unique chaque n-uplets d'une relation.

**Règles** : choisir comme clé primaire la plus petite des clés candidates (au niveau de la taille).

**Contrainte d’unicité** : une clé primaire ne peut pas prendre 2 fois la même valeur. La valeur figure au maximum une fois.

Par exemple la base de données **Lycée** contient 5 relations :

ELEVE(nom, prenom, NIP, adresse, telephone, age)
OPTION(id_option, nom_option, departement)
INSCRIPTION(NIP, id_option, classe)
AFFECTATION(id_option, classe, id_livre)
LIVRE(id_livre, titre_livre, auteur, année_de_publication)

Une **clé étrangère** identifie une colonne ou un ensemble de colonnes d'une table comme référençant une colonne ou un ensemble de colonnes d'une autre table (la table référencée).

**Exemple** : id_option est la clé primaire de la table OPTION et elle est une clé étrangère de la table AFFECTATION.

**Contrainte entre relation (intégrité référentielle)** : seules les valeurs présentes dans la table de référence sont autorisées.

