# **Structure de données : Graphes**

Nous avons vu dans le cours qu'il est possible d'implémenter les graphes avec des dictionnaires. Une autre façon de faire, serai de définir une classe ```Graphe```.

## **Présentation de la classe**

Voici la liste des fonctions de notre classe ```Graphe``` :

```python
class Graphe:
    # CONSTRUCTEUR
    def __init__(self, sommets, arcs):
    # GETTERS
    def get_sommets(self):
    def get_arcs(self):
    def voisin(self, s):
    # METHODES
    def matrice(self):
```

Le but de ce TP est de créer la classe ```Graphe``` (qui permettra de travailler sur des **Graphes Orientés Pondérés**).

### **Les attributs**

Cette classe sera constituée de 2 attributs :

- Un premier attribut qui sera la liste des Sommets du Graphes (cette liste sera triée dans l'ordre croissant),
- un second attribut qui sera l'ensemble de tous les Arcs du Graphes ; un arc sera représenté par un couple de 3 éléments : (sommet sortant, sommet entrant, poids).

### **Les méthodes**

Afin de pouvoir utiliser efficacement notre classe, il va nous falloir dans un premier temps définir le **constructeur** ainsi que les **getters** :

- ```get_sommets()``` : renvoie la liste des sommets du graphe,
- ```get_arcs()``` : renvoie la liste des arcs du graphe,
- ```voisin(s)``` : renvoie la liste des voisins du sommet s dans le graphe.

Après avoir construit la base de la classe, nous pouvons nous intéresser sur la fonction permettant de récupérer la matrice des poids du Graphe :

```
Paramètres :
    Aucun
Renvoie :
    (dict) un dictionnaire des poids
Algorithme :
    res <- dictionnaire vide
    Pour tout S1 dans la liste des sommets de G:
        res[S1] <- dictionnaire vide
        Pour tout S2 dans la liste des sommets de G:
            si S2 == S1:
                res[S2] = None
            sinon:
                res[S2] = valeur infinie
    Pour tout a dans la liste des arcs de G:
        res[a[0]][a[1]] = a[2]
    return res
```

**A faire :** Créer la classe Graphe.