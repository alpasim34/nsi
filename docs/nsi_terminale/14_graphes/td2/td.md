# **Structure de données : Graphes**

## **Exercice 1 - le voyage**

Emmanuel veut faire un tour avec sa compagne sur chaque continent du monde : **Brasilia**, **San Francisco**, **Paris**, **Dakar**, **Pekin**, **Sydney**. Du fait de la distance entre ces grandes villes, nos 2 aventuriers n'ont d'autre choix que de prendre l'avion.

```
Question 1 - Déssiner un graphe non orienté représentant pour chaque sommet une ville cité dans le parcours de Michel et sa compagne. On supposera que toute les villes sont reliées entre elles par les voix aériennes.

Question 2 - Quel est ordre de ce graphe et que peut on dire des sommets ainsi que de la matrice d'adjacence du graphe ?
```

Après s'être concertés, nos 2 protagonistes décident d'ajouter à leur liste 2 villes supplémentaire : **Moskou** & l'**Egypte** malgré le fait que pour aller à **Moskou**, on ne peut y accéder que en partant de **Paris** et aussi malgré le fait que pour aller en **Egypte**, on ne peut passer que par **Dakar**.

```
Question 3 - Que peut on dire des sommets correspondant à Paris et Dakar ?

Question 4 - Avec l'ajout de ces sommets, avons nous une chaine hamiltonienne ? une chaine eulérienne ? Si oui, en donner une.
```

Maintenant qu'Emmanuel et sa compagne savent dans quel ville ils vont séjourner, il y a 2 points à aborder :

- le temps de vol,
- le cout de déplacement d'une ville à une autre.

### **Le temps de vol**

Après sa journée de travail, Emmanuel va voir dans une agence de voyage les trajets possibles par rapport aux villes citées plus haut et voici ce qu'il trouve :

- De **Brasilia**, nous pouvons prendre l'avion et aller jusque **Paris** (6 heures) et **San Francisco** (5 heures) ;
- De **San Francisco**, nous pouvons prendre l'avion et aller jusque **Pekin** (3 heures), **Paris** (4 heures) et **Sydney** (3 heures) ;
- De **Pekin**, nous pouvons prendre l'avion et aller jusque **Dakar** (7 heures), **Sydney** (3 heures) ;
- De **Dakar**, nous pouvons prendre l'avion et aller jusque **Paris** (2 heures) et l'**Egypte** (1 heure) ;
- De **Paris**, nous pouvons prendre l'avion et aller jusque **Dakar** (2 heures), **Moskou** (5 heures) et **Brasilia** (6 heures) ;
- De **Sydney**, nous pouvons prendre l'avion et aller jusque **San Francisco** (3 heures) et **Dakar** (3 heures) ;
- De **Moskou**, nous pouvons prendre l'avion et retourner sur **Paris** (5 heures) ;
- De l'**Egypte**, nous pouvons prendre l'avion et retourner sur **Dakar** (1 heure).

```
Question 4 - Dessiner le graphe orienté selon les informations données.

Question 5 - Donner l'ordre du graphe et donner dans un tableau tous les degrés entrants et sortants de tous les sommets du graphe.

Question 6 - Y a t'il des chaines eulériennes ? hamiltoniennes ? Si oui, en donner une.

Question 7 - Donner la matrice de POIDS du graphe.

Question 8 - Sachant qu'on part de San Francisco, donner le résultat de l'appel de l'algorithme de Dijkstra vu en cours sur le graphe.
```

### **Le cout de vol**

En utilisant l'algorithme de Dijkstra, Emmanuel parvient a avoir un itinéraire selon la durée, sa compagne étant économe, il faut voir si ce trajet est le moins cher. Voici donc les tarifs :

- **Brasilia** -> **Paris** : 150 € ;
- **Brasilia** -> **San Francisco** : 75 € ;
- **San Francisco** -> **Pekin** : 125 € ;
- **San Francisco** -> **Paris** : 90 € ;
- **San Francisco** -> **Sydney** : 200 € ;
- **Pekin** -> **Dakar** : 60 € ;
- **Pekin** -> **Sydney** : 100 € ;
- **Dakar** -> **Paris** : 50 € ;
- **Dakar** -> **Egypte** : 60 € ;
- **Paris** -> **Dakar** : 50 € ;
- **Paris** -> **Moskou** : 70 € ;
- **Paris** -> **Brasilia** : 150 € ;
- **Sydney** -> **San Francisco** : 200 € ;
- **Sydney** -> **Dakar** : 110 € ;
- **Moskou** -> **Paris** : 70 € ;
- **Egypte** -> **Dakar** : 60 €.

```
Question 9 - A combien revient le chemin vu dans la question 8 ?

Question 10 - Refaire l'application de l'algorithme de Dijkstra avec en pondération les couts à la place des temps de vol. Le chemin est il différent de celui vu avant ?
```