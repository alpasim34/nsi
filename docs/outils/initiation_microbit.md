# Ressources NSI - Outils


## Prise du microcontrôleur micro:bit

Un microcontrôleur est un processeur associé à une mémoire, pour stocker programmes et données, et d'entrées/sorties compatibles avec la plupart des circuits électroniques. Ils sont utilisés pour commander des actionneurs, acquérir les données de capteurs ou plus généralement piloter les circuits électroniques.

1\. Installer l'environnement de programmation intégré Mu Editor : [md](../outils/logiciels.md)

2\. Diaporama : [odp](./initiation_microbit/initiation_MicroBit.odp) ou [pdf](./initiation_microbit/initiation_MicroBit.pdf)

3\. Fiche sur une première approche de la carte micro:bit : [odt](./initiation_microbit/109_Carte_MicroBit.odt) ou [pdf](./initiation_microbit/109_Carte_MicroBit.pdf)

4\. Faire les activités proposées dans les liens à la fin du diaporama

5\. Fiche sur le branchement de capteurs et l'acquisition analogique : [odt](./initiation_microbit/110_Carte_MicroBit_Acquisition_analogique.odt) ou [pdf](./initiation_microbit/110_Carte_MicroBit_Acquisition_analogique.pdf)
