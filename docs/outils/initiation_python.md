# Ressources NSI - Outils


## Prise en main de la programmation Python

Il s'agit ici d'aborder le programmation en Python. Les notions essentielles sont présentées, ainsi que l'utilisation d'un environnement de programmation et les bonnes pratiques.

1\. Installer l'environnement de programmation intégré Thonny : [md](../outils/logiciels.md)

2\. Diaporama : [odp](./initiation_python/initiation_Python.odp) ou [pdf](./initiation_python/initiation_Python.pdf)

3\. Tester toutes les lignes de codes présentées dans le diaporama

4\. Les bonnes pratiques : [odp](./initiation_python/bonnes_pratiques_Python.odp) ou [pdf](./initiation_python/bonnes_pratiques_Python.pdf)

5\. Fiche permettant de répertorier les erreurs rencontrées : [odt](./initiation_python/fiche_liste_erreurs.odt) ou [pdf](./initiation_python/fiche_liste_erreurs.pdf)

6\. Défis en sciences et vie de la terre, mathématiques, physique-chimie, lettres et langues : [odp](./initiation_python/defis_Python.odp) ou [pdf](./initiation_python/defis_Python.pdf)

-  [Mémento python](./initiation_python/mementopython3.pdf) et [abrégé dense](./initiation_python/abregepython.pdf) de Laurent Pointal.
- Fiche utilisation de python sous Windows : [pdf](./initiation_python/python_utilisation_windows.pdf) ou [odt](./initiation_python/python_utilisation_windows.odt)
- Fiche utilisation de python sous Linux : [pdf](./initiation_python/python_utilisation_linux.pdf) ou [odt](./initiation_python/python_utilisation_linux.odt)


# Programme python type, fonction type, module type et doctests

Dossier compressé à télécharger : [zip](./initiation_python/programmes_python_types/programmes_python_types.zip)
