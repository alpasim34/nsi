# Spécialité Numérique et Sciences Informatiques

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------

## Présentation

Supports de cours, TD et TP pour la spécialité NSI.

* [Classe de première](./nsi_premiere/index_premiere.md)
* [Classe de terminale](./nsi_terminale/index_terminale.md)

Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/nsi)

D'autres collègues ont mis en ligne des cours détaillés et fournis en activités :

* [Dépôt Framagit de Ghesquière Cécile en 1NSI](https://framagit.org/CecGhesq/1_nsi_21)
* [Dépôt Framagit de Ghesquière Cécile en TNSI](https://framagit.org/CecGhesq/t_nsi_21)
* [Dépôt Framagit de Mieszczak Christophe](https://framagit.org/tofmzk/informatique_git/-/tree/master/)
* [Infoforall](https://www.infoforall.fr/) : l'informatique pour tous
* [Pixees](https://pixees.fr/informatiquelycee/) : informatique au lycée
* [Lycée Blaise Pascal de Clermont Ferrand](https://info.blaisepascal.fr/) : l'informatique, c'est fantastique !
* [Lycée Angellier de Dunkerque](http://vfsilesieux.free.fr/)
* [Lycée Jean Moulin de Dradignan](https://isn-icn-ljm.pagesperso-orange.fr/)
* [Page de Laurent Pointal, Ingénieur d'Études CNRS au LIMSI d'Orsay](https://perso.limsi.fr/pointal/python:accueil)


## Les décodeuses du Numérique

![](assets/index-0cc56078.png)

12 portraits de chercheuses, enseignantes-chercheuses et ingénieures dans les sciences du numérique, croquées par le crayon de Léa Castor. L'Institut des sciences de l'information et de leurs interactions (INS2I) du CNRS met en avant la diversité des recherches en sciences du numérique à travers l'implication des femmes dans ce domaine.

[La page web](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique)

[La BD en consultation libre](https://www.calameo.com/read/006841715804996467dcf?authid=cG5djzzVzuiW)

[La BD en pdf](./les_decodeuses_du_numerique_web.pdf)


## Programmes de la NSI

* Programme de première NSI : [pdf](./nsi_premiere/Programme_1NSI.pdf)
* Programme de terminale NSI : [pdf](./nsi_terminale/Programme_TNSI.pdf)
* Points du programme de terminale qui ne figurent pas aux épreuves écrite et pratique: [md](./nsi_terminale/adaptation_epreuves_ecrite_pratique.md)


## Le baccalauréat NSI

* Résumé des épreuves 2022 en première et en terminale : [pdf](./Bac_NSI.pdf)
* Baccalauréat 2022 :
    * Calendrier des épreuves : [pdf](./Calendrier_epreuves_bac_2022.pdf)
    * Coefficients : [jpg](./Coefficients_epreuves_bac_2022.jpg)
    * Calculer sa note : [pdf](./calcul_note_bac_2022.pdf)
* Baccalauréat 2023 :
    * Coefficients : [jpg](./Coefficients_epreuves_bac_2023.jpg)
    * Calculer sa note : [pdf](./calcul_note_bac_2023.pdf)
* Sujets des épreuves écrites de première du baccalauréat 2020 et 2021 :
    - Banque nationale des sujets de première : [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_premiere/Sujets_Bac)
* Sujets des épreuves écrites et pratiques de terminale :
    - Banque nationale des épreuves pratiques de terminale : [lien](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi) et [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_terminale/Sujets_Bac/epreuves_pratiques)
    - Sujets écrits complets et fractionnés par thèmes : [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_terminale/Sujets_Bac/epreuves_ecrites)
* Grand Oral :
    - document de présentation : [pdf](./Grand_oral_Igesr_Document_integral_0.pdf)
    - foire aux questions : [pdf](./Grand-oral_FAQ_enseignants.pdf)
    - grille d'évaluation : [pdf](./Grand-oral_grille_d_evaluation.pdf)


## L'orientation en informatique à lille

Présentation réalisée par Amélie Marette, MEEF2 : [pdf](./diapo_orientation_lille.pdf) ou [odp](./diapo_orientation_lille.odp)


## Outils pour la NSI

* Logiciels : [md](./outils/logiciels.md)
* Prise en main du langage Python : [md](./outils/initiation_python.md)
* Modules python : [md](./outils/modules_python.md)
* Prise en main du microcontrôleur micro:bit : [md](./outils/initiation_microbit.md)
* Utilisation d'un drone Tello avec Python : [md](./outils/drone_tello.md)
* Prise en main de la rédaction en markdown : [md](./outils/initiation_markdown.md)
* Les quaternions : [md](./outils/quaternions.md)


## Bibliographie

* [Wikipedia](https://fr.wikipedia.org)
* [Openclassrooms](https://openclassrooms.com/fr/)
* [Université de Lille - Formation à l'enseignement de l'informatique](http://fe.fil.univ-lille1.fr)
* [SUPINFO International University](https://www.supinfo.com/)
* Ellipse : Spécialité Numérique et sciences informatiques : 30 leçons avec exercices corrigés - Première - Nouveaux programmes
* Ellipse : Spécialité Numérique et sciences informatiques : 24 leçons avec exercices corrigés - Terminale - Nouveaux programmes
* Nathan : Interros des Lycées Numérique Sciences Informatiques - Terminale
* Hachette : Numérique et Sciences Informatiques 1re Spécialité - Livre élève - Ed. 2021

--------------------
