

## Algorithmique - algorithmes classiques à connaître

Un certains nombres d'algorithmes fondamentaux sont à maîtriser. Leur étude donnera une meilleure compréhension et maîtrise de la programmation. Ces algorithmes seront abordés au fur et à mesure des deux années de NSI.

- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)


## Python - Constructions élémentaires - Programmation procédurale

Le cours précédent est complété avec la programmation structurée et quelques aspects modulaires.

- TP-cours sur les fonctions : [pdf](./03_python_constructions_elementaires/Cours_Python_03_Fonctions.pdf) ou [odt](./03_python_constructions_elementaires/Cours_Python_03_Fonctions.odt)
- Compléments sur la création de modules : [pdf](./03_python_constructions_elementaires/Complements_creer_module.pdf) ou [odt](./03_python_constructions_elementaires/Complements_creer_module.odt)
- Fiche exercices sur les fonctions : [pdf](./03_python_constructions_elementaires/Exercices_Python_02_Fonctions_et_turtle.pdf) ou [odt](./03_python_constructions_elementaires/Exercices_Python_02_Fonctions_et_turtle.odt)


## Systèmes d’exploitation

- Diapo cours : [pdf](./01_systemes_exploitation/cours_OS.pdf) ou [odp](./01_systemes_exploitation/cours_OS.odp)
- Extrait de licence Windows : [html](./01_systemes_exploitation/UseTerms_Retail_Windows_10_French.htm)
- Frise histoire de Windows : [pdf](./01_systemes_exploitation/Frise_Histoire_Windows.pdf)
- TP version Linux : [pdf](./01_systemes_exploitation/TP_OS_linux.pdf) ou [odt](./01_systemes_exploitation/TP_OS_linux.odt)
- Jeu pour apprendre les commandes Linux : [Terminus en français](https://luffah.xyz/bidules/Terminus/) et [Terminus en VO](https://www.mprat.org/Terminus/)
- TP sur le gestion des droits d'accès aux fichiers et dossiers sous Linux : [pdf](./01_systemes_exploitation/TP_droits.pdf) ou [odt](./01_systemes_exploitation/TP_droits.odt)
- Fiche sur le gestion des droits d'accès aux fichiers et dossiers sous Linux : [pdf](./01_systemes_exploitation/diapo_droits.pdf) ou [odp](./01_systemes_exploitation/diapo_droits.odp)

<!-- - TP version Windows : [pdf](./01_systemes_exploitation/TP_OS.pdf) ou [odt](./01_systemes_exploitation/TP_OS.odt) -->


## Python - structures de données : les séquences - chaînes de caractères

- Cours d'introduction aux séquences en Python : [pdf](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.pdf) ou [odt](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.odt)
- TP-cours sur les chaînes de caractères : [pdf](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.pdf) ou [odt](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.odt)
- Compléments sur le formatage des chaînes de caractères : [pdf](./04_python_sequences/Complements_chaines.pdf) ou [odt](./04_python_sequences/Complements_chaines.odt)
- Fiche d'exercices pratiques Python sur les chaînes de caractères : [pdf](./04_python_sequences/Exercices_Python_04_chaines.pdf) ou [odt](./04_python_sequences/Exercices_Python_04_chaines.odt)
- Fiche d'exercices écrits Python sur les chaînes de caractères : [pdf](./04_python_sequences/exos_chaines.pdf) ou [odt](./04_python_sequences/exos_chaines.odt)
- Mini-projet ASCII-ART : [md](../projets/ascii_art/Ascii_Art.md)


## Python - structures de données : les séquences - n-uplets

- TP-cours sur les n-uplets : [pdf](./04_python_sequences/Cours_Python_04c_Les_sequences_tuples.pdf) ou [odt](./04_python_sequences/Cours_Python_04c_Les_sequences_tuples.odt)
- Fiche d'exercices pratiques Python sur les n-uplets : [pdf](./04_python_sequences/exos_tuples.pdf) ou [odt](./04_python_sequences/exos_tuples.odt)


## Architecture de l'ordinateur et Algorithmique - calcul mécanique et algorithmes

Les algorithmes modernes de calcul reposent sur une longue accumulation d'idées mathématiques et de mises en oeuvres techniques dont le but est d'automatiser le calcul et notamment les opérations de base : addition, soustraction, multiplication et addition. Il ne faudrait pas croire que ce domaine des sciences est ancien et que l'Homme le maîtrise entièrement. En effet l'ordinateur est une invention récente et le dernier algorithme de multiplication pour les très grands entiers a été trouvé en 2007 par le mathématicien suisse Martin Fürer. Les enjeux sont très importants puisque par exemple la sécurisation de données est liée à la rapidité des machines à les crypter et les décrypter à l'aide d'algorithmes de calcul.

- TP sur le calcul mécanique et les algorithmes de calcul : [pdf](./08_architecture_ordinateur/03_calcul_mecanique/TP_calcul_mecanique_algorithmes.pdf) ou [odt](./08_architecture_ordinateur/03_calcul_mecanique/TP_calcul_mecanique_algorithmes.odt)
    - Vidéo de démonstration de l'utilisation du soroban japonais : [émission Japan in Motion](./08_architecture_ordinateur/03_calcul_mecanique/Demonstration_du_soroban_japonais.mp4)
    - Exemple de bâtons de Neper : [pdf](./08_architecture_ordinateur/03_calcul_mecanique/Batons_de_Neper.pdf) ou [ods](./08_architecture_ordinateur/03_calcul_mecanique/Batons_de_Neper.ods)


## Représentation des données : les entiers naturels

- Diapo cours histoire de la numération binaire : [pdf](./05_representation_donnees/01_présentation/presentation_numeration.pdf) ou [odp](./05_representation_donnees/01_présentation/presentation_numeration.odp)
- Archives de l'Académie Royale des Sciences avec l'article de Leibnitz : [pdf](./05_representation_donnees/01_présentation/Archives_academie_des_sciences.pdf)
- TP_cours sur les entiers naturels : [pdf](./05_representation_donnees/02_numeration_entiers_naturels/les_entiers_naturels.pdf) ou [odt](./05_representation_donnees/02_numeration_entiers_naturels/les_entiers_naturels.odt)
- Fiche exercices sur les entiers naturels : [pdf](./05_representation_donnees/02_numeration_entiers_naturels/exos_entiers_non_signes.pdf) ou [odt](./05_representation_donnees/02_numeration_entiers_naturels/exos_entiers_non_signes.odt)


## Interaction Homme-Machine sur le web
- TP sur quelques notions de html et javascript sous la forme d'un site web sur l'algèbre de Boole : [zip](./08_architecture_ordinateur/01_algebre_booleenne/site_algebre_Boole.zip)
- TP construction d'un site web statique (à la maison) : [pdf](./06_ihm_web/01_construction_site_web/construction_site_web.pdf) ou [odt](./06_ihm_web/01_construction_site_web/construction_site_web.odt)
    * Ressources pour le TP site web : [zip](./06_ihm_web/01_construction_site_web/images.zip)
- Un site web en flexbox (à la maison) : [pdf](./06_ihm_web/02_site_flexbox/site_web_Flexbox.pdf) ou [odt](./06_ihm_web/02_site_flexbox/site_web_Flexbox.odt)
    * Ressources pour le TP flexbox : [zip](./06_ihm_web/02_site_flexbox/images.zip)
    * Ajout de pages au site : [pdf](./06_ihm_web/02_site_flexbox/Ajout_pages_web.pdf) ou [odt](./06_ihm_web/02_site_flexbox/Ajout_pages_web.odt)
- Compléments sur la couleur en CSS : [pdf](./06_ihm_web/02_site_flexbox/couleurs_en_CSS.pdf) ou [odt](./06_ihm_web/02_site_flexbox/couleurs_en_CSS.odt)

  <!-- - TP-cours introductif Internet, Web et site web : [pdf](./06_ihm_web/00_introduction_WEB/TP_web.pdf) ou [odt](./06_ihm_web/00_introduction_WEB/TP_web.odt)
    * Ressources pour le TP introductif : [zip](./06_ihm_web/00_introduction_WEB/tp_web.zip) -->


## Fonctionnement électrique de l'ordinateur - Circuits combinatoires

- Notions de cours :
    * Fiche décrivant les principales fonctions logiques utilisées en électronique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Electronique_et_logique.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Electronique_et_logique.odt)
    * Fiche décrivant les signaux électriques transmis en informatique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Signaux_numériques.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Signaux_numériques.odt)
    * Article sur la problématique du Dark Silicon [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/Dark_Silicon.pdf)
- Fiche TD sur les circuits logiques : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_TP_Representation_electrique_information_papier.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_TP_Representation_electrique_information_papier.odt)
    * Circuit de l'additionneur 4 bits avec registres : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit.pdf)
    * Aide pour étudier le fonctionnement de l'additionneur série : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_serie.pdf) ou [odp](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_serie.odp)
- TP Python sur les circuits combinatoires : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques_python.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques_python.odt)
    * Ressources : [py](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques.py)
- Construire un ordinateur de A à Z : [lien](https://nandgame.com/)
- Schéma électronique de l'UAL 4 bits 74181 : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/ual_4bits_74181.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/ual_4bits_74181.odp)


## Python - structures de données : les séquences - tableaux

- TP-cours sur les tableaux : [pdf](./04_python_sequences/Cours_Python_04d_Les_sequences_listes.pdf) ou [odt](./04_python_sequences/Cours_Python_04d_Les_sequences_listes.odt)


## Architecture réseau : les bases

- Diapo cours sur l'adressage, le protocole TCP-IP, l'encapsulation des données et la topologie des réseaux : [pdf](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.pdf) ou [odp](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.odp)
  * Vidéo sur l'histoire d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/0_Historique_et_Principe.mp4)
  * Vidéo sur le fonctionnement d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/1_Internet_Comment_ca_marche.mp4)
  * Vidéo sur le protocole TCP-IP : [mp4](./07_architecture_reseau/01_cours_reseau/3_MOOC_SNT_Internet_Protocole_TCP_IP.mp4)
- Fiche sur les protocoles IP et TCP : [pdf](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf) ou [odt](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.odt)
- TP sur la construction et la configuration d'un réseau type internet : [pdf](./07_architecture_reseau/02_contruction_configuration_reseau/Filius_NSI.pdf)


## Représentation des données : les entiers relatifs

- TP-cours sur les entiers relatifs : [pdf](./05_representation_donnees/03_entiers_relatifs/Cours_Representation_nombres_relatifs.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/Cours_Representation_nombres_relatifs.odt)
- Videos pour comprendre le codage des entiers naturels :
  * fonctionnement d'un odomètre : [mp4](./05_representation_donnees/03_entiers_relatifs/mechanical-odometer-1.mp4)
  * odomètre à rebours : [mp4](./05_representation_donnees/03_entiers_relatifs/registre_calculatrice_cpt_a_rebours.mp4)
- Fiche exercices sur les entiers relatifs : [pdf](./05_representation_donnees/03_entiers_relatifs/exos_entiers_signes.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/exos_entiers_signes.odt)
- TP Python sur la représentation des entiers : [pdf](./05_representation_donnees/03_entiers_relatifs/TP_Représentation_des_nombres.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/TP_Représentation_des_nombres.odt)


## Python : les dictionnaires

Les dictionnaires sont des structures de données mutables qui permettent d'identifier chacun de leurs éléments grâce à une clé, qui peut être une chaîne de caractères, un nombre, ...

- TP-cours : [pdf](./11_python_dictionnaires/Cours_Python_04e_Les_dictionnaires.pdf) ou [odt](./11_python_dictionnaires/Cours_Python_04e_Les_dictionnaires.odt)


## Traitement des données en table

Introduction au traitement de données en utilisant les tableaux (listes) à deux dimensions pour représenter les tables.

- TP-cours sur les fichiers CSV : [pdf](./09_traitement_donnees/ouverture_fichiers_csv.pdf) ou [odt](./09_traitement_donnees/ouverture_fichiers_csv.odt)
- Cours sur les données et leur structuration : [pdf](./09_traitement_donnees/cours_donnees_structuration.pdf) ou [odt](./09_traitement_donnees/cours_donnees_structuration.odt)
- Activités sur le traitement des données : [pdf](./09_traitement_donnees/traitement_donnees_table.pdf) ou [odt](./09_traitement_donnees/traitement_donnees_table.odt)
  * Ressources : [zip](./09_traitement_donnees/pokemon_stats.zip)


## Algorithmique - Parcours séquentiel d'un tableau

Reprendre en Python tous les algorithmes de la fiche ci-dessous jusqu'à la division euclidienne.

- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)
<!-- - Travail pratique autour du calcul de moyenne, S -->

## Algorithmique - Les tris

Deux tris sont à l'étude. Lors du tri par sélection, à chaque étape, on sélectionne tout simplement le plus petit élément restant que l'on place à la suite dans la liste triée. Lors du tri par insertion, chaque élément est tour à tour inséré à sa place dans la liste triée.

- Illustration du tri par sélection : [pdf](./10_algorithmique/02_tris/SR_01_tri_selection.pdf) ou [odp](./10_algorithmique/02_tris/SR_01_tri_selection.odp)
- Illustration du tri par insertion : [pdf](./10_algorithmique/02_tris/SR_02_tri_insertion.pdf) ou [odp](./10_algorithmique/02_tris/SR_02_tri_insertion.odp)
- Travail pratique autour des tris : [md](./10_algorithmique/TP_algo_02_tris.md)


## Algorithmique - Complexité

- Introduction à l'algorithmique : [pdf](./10_algorithmique/cours_algo_1_intro.pdf) ou [odt](./10_algorithmique/cours_algo_1_intro.odt)
- Complexité en temps : [pdf](./10_algorithmique/cours_algo_2_complexite.pdf) ou [odt](./10_algorithmique/cours_algo_2_complexite.odt)
- Correction d'un algorithme : [pdf](./10_algorithmique/cours_algo_3_correction.pdf) ou [odt](./10_algorithmique/cours_algo_3_correction.pdf)


## Algorithmique - Recherche dichotomique

- Illustration de la recherche dichotomique : [pdf](./10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.pdf) ou [odp](./10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.odp)
- Travail pratique autour de la recherche dichotomique : [md](./10_algorithmique/TP_algo_03_recherche_dichotomique.md)


## Algorithmique : vers la résolution des problèmes complexes

- Travail pratique autour de l'algorithme des plus proches voisins et les algorithmes gloutons : [md](./10_algorithmique/TP_algo_04_algorithmes_intelligents.md)


## Python - Démarrer

- Diapo cours : [pdf](./02_python_demarrer/Cours_Python_01_presentation.pdf) ou [odp](./02_python_demarrer/Cours_Python_01_presentation.odp)
- TP-cours pour démarrer en Python : [pdf](./02_python_demarrer/Cours_Python_01_Demarrer_en_Python.pdf) ou [odt](./02_python_demarrer/Cours_Python_01_Demarrer_en_Python.odt)
- Frise histoire des langages de programmation : [pdf](./02_python_demarrer/Frise_Histoire_Langages.pdf)


## Représentation des données : les nombres réels

- TD-cours : [pdf](./05_representation_donnees/04_reels/flottants_cours.pdf) ou [odt](./05_representation_donnees/04_reels/flottants_cours.odt)
- Illustrations de cours : [pdf](./05_representation_donnees/04_reels/flottants_cours_illustrations.pdf) ou [odp](./05_representation_donnees/04_reels/flottants_cours_illustrations.odp)
- Fiche exercices : [pdf](./05_representation_donnees/04_reels/flottants_exercices.pdf) ou [odt](./05_representation_donnees/04_reels/flottants_exercices.odt)


## Interaction client-serveur (protocoles TCP, HTTP, HTTPS)

La mise en pratique nécessite l'installation de [Wireshark](https://www.wireshark.org/). Il est disponible sous Linux, macOS et Windows. Sous ce dernier l'installation du driver [Npcap](https://nmap.org/npcap/) sera nécessaire pour acquérir les données échangées entre deux machines différentes.

L'activité peut très bien se faire en local sur la même machine. Elle sert alors à la fois de serveur et de client. Les applications serveur et client seront donc lancées en parallèle. Il faut pour cela utiliser les adresses de loopback. L'adresse IP client sera 127.0.0.1 et l'adresse IP serveur sera 127.0.0.X avec X entre 2 et 254, par exemple 127.0.0.20.

Enfin, l'activité pourrait être simulée sous [Filius](https://www.lernsoftware-filius.de/Herunterladen) à l'aide du mini-réseau construit dans le cours précédent.

- Notions de cours :
  * Fiche décrivant la mise en place d'un serveur Python : [pdf](./06_ihm_web/03_client_serveur/04a_Fiche_eleve_Serveur_python.pdf) ou [odt](./06_ihm_web/03_client_serveur/04a_Fiche_eleve_Serveur_python.odt)
  * Fiche décrivant la mise en place d'un serveur Python sécurisé : [pdf](./06_ihm_web/03_client_serveur/04e_Fiche_eleve_Serveur_python_securise.pdf) ou [odt](./06_ihm_web/03_client_serveur/04e_Fiche_eleve_Serveur_python_securise.odt)
  * Fiche rappelant quelques notions sur le protocole HTTP : [pdf](./06_ihm_web/03_client_serveur/04c_Activite_SNT_Fiche_eleve_HTTP.pdf) ou [odt](./06_ihm_web/03_client_serveur/04c_Activite_SNT_Fiche_eleve_HTTP.odt)
- Mise en pratique autour d'un site web statique puis dynamique :
  * Sujet : [pdf](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.pdf) ou [odt](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.odt)
  * Fichiers nécessaires [zip](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.zip)


## Architecture réseau : protocole du bit alterné

- Notions de cours : [pdf](./07_architecture_reseau/03_bit_alterne/bit_alterne.pdf) ou [odp](./07_architecture_reseau/03_bit_alterne/bit_alterne.odp)


## Représentation des données : les caractères

- Notions de cours et mise en pratique : [pdf](./05_representation_donnees/05_caracteres/representation_des_caracteres.pdf) ou [odt](./05_representation_donnees/05_caracteres/representation_des_caracteres.odt)
- Illustrations de cours : [pdf](./05_representation_donnees/05_caracteres/illustrations_cours.pdf) ou [odp](./05_representation_donnees/05_caracteres/illustrations_cours.odp)
- exercice HTML et charset : [pdf](./05_representation_donnees/05_caracteres/HTML_charset.pdf) ou [odt](./05_representation_donnees/05_caracteres/HTML_charset.odt)
- Notions et activités autour des fichiers textes et de leur encodage : [pdf](./05_representation_donnees/05_caracteres/ouverture_fichiers_texte.pdf) ou [odt](./05_representation_donnees/05_caracteres/ouverture_fichiers_texte.odt)


## Mini projet guidé : la machine à inventer des mots.

- Vidéo introductive : [Science étonnante](https://sciencetonnante.wordpress.com/2015/11/06/la-machine-a-inventer-des-mots-version-ikea/)
- Guide Python : [pdf](../projets/machine_mots/machine_inventer_mots.pdf) ou [odp](../projets/machine_mots/machine_inventer_mots.odp)
  * Ressources : [zip](../projets/machine_mots/ressources_mots.zip)
