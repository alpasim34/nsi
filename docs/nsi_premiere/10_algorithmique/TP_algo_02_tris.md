# Les tris


Trier des données est une opération de base présente dans tout traitement. Disposer d'algorithmes de tri efficaces à l'heure du Big Data est devenu crucial. Le cours de NSI commence par introduire les tris classiques. Ce ne sont pas les plus performants mais ils sont simples et comprendre leur mécanisme permet d'apprendre à construire des algorithmes plus complexes. Par ordre d'efficacité : tri par sélection (selection sort), tri à bulles (bubble sort), tri par insertion (insertion sort), tri fusion (merge sort), tri par tas (heapsort), tri rapide (quicksort). Insertion est très rapide sur des données de petite taille. Rapide est considéré comme le meilleur algorithme sur des données de grande taille.


## I. Tri par sélection

C'est un tri sur place, c'est-à-dire que la liste non triée est modifiée au fur et à mesure du tri lors de la mise en ordre. Il n'y a donc qu'une seule liste en mémoire de l'ordinateur.

Le tri est illustré sur le document : [pdf](./02_tris/SR_01_tri_selection.pdf)

Le principe est de diviser la liste en deux parties : une partie triée et une partie non triée. On recherche l'élément le plus petit restant dans la partie non triée de la liste et ont le met à la suite de la partie triée de la liste.

Au début de l'algorithme on se place sur l'élément 0 qui est supposé ne pas être à sa place. On recherche l'élément le plus petit, de l'élément 0 à la fin de la liste. Une fois trouvé on l'échange avec l'élément 0. L'élément 0 est donc à sa place maintenant.

On se place ensuite sur l'élément 1 qui est supposé ne pas être à sa place. On recherche l'élément le plus petit, de l'élément 1 à la fin de la liste. Une fois trouvé on l'échange avec l'élément 1. L'élément 1 est donc à sa place maintenant.

On se place ensuite sur l'élément 2 qui est supposé ne pas être à sa place. On recherche l'élément le plus petit, de l'élément 2 à la fin de la liste. Une fois trouvé on l'échange avec l'élément 2. L'élément 2 est donc à sa place maintenant.

A cette étape du tri, la partie de la liste correspondant aux éléments 0, 1 et 2 est triée. Le reste est non trié.

On poursuit ainsi jusqu'à avoir traité tous les éléments les uns après les autres.

Un algorithme possible est le suivant.

```
BOUCLE pour i allant de 0 à longueur de la liste - 1
    plus_petit_non_en_place ← i
    BOUCLE pour j allant de i à longueur de la liste - 1
        SI élément j de liste_nombres est strictement inférieur à l'élément plus_petit_non_en_place de liste_nombres
            plus_petit_non_en_place ← j
    temp ← élément i de liste_nombres
    élément i de liste_nombres ← élément plus_petit_non_en_place de liste_nombres
    élément plus_petit_non_en_place de liste_nombres ← temp
```

1\. Coder l'algorithme en Python et le tester sur une liste de 30 nombres aléatoires entre 0 et 100. Cette liste sera générée par compréhension avec `liste_nombres = [random.randint(0, 100) for i in range(30)]` grâce au module `random`.

2\. Montrer que la complexité en temps dans le pire des cas de cet algorithme est $`\mathrm{T(n) = 5.n^2 + n}`$, avec $`\mathrm{n}`$ la longueur de la liste.

3\. Montrer que l'algorithme se termine.

4\. Montre que l'algorithme est correct.
\. Exécuter l'algorithme pour différentes valeurs de n. On prendra `n = 20`, `n = 40`, `n = 60`, ..., jusque `n = 200`. Noter les valeurs obtenues pour le temps d'exécution $`\mathrm{\Delta t}`$. On travaillera dans le pire des cas, c'est à dire lorsque l'on trie une liste déjà triée dans l'autre sens.

6\. Tracer le nuage de points $`\mathrm{\Delta t = f(n)}`$. Quel type de courbe obtient-on ? Est-ce attendu ? Expliquer.


## II. Tri par insertion

C'est aussi un tri sur place.

Le tri est illustré sur le document : [pdf](./02_tris/SR_02_tri_insertion.pdf)

Comme pour le tri par sélection, la liste est divisée en deux parties : triée et non triée.

Au début de l'algorithme on considère que l'élément 0 est la partie triée de la liste et on se place sur l'élément 1 qui est supposé ne pas être à sa place. On recherche sa place dans la liste triée et on l'y positionne. Pour cela on doit décaler tous les éléments plus grand que lui dans la liste triée pour lui faire sa place.

On se place ensuite sur l'élément 2 qui est supposé ne pas être à sa place. On recherche sa place dans la liste triée et on l'y positionne. Pour cela on doit décaler tous les éléments plus grand que lui dans la liste triée pour lui faire sa place.

A cette étape du tri, La partie de la liste correspondant aux éléments 0, 1 et 2 est triée. Le reste est non trié.

On poursuit ainsi jusqu'à avoir traité tous les éléments les uns après les autres.

Un algorithme possible est le suivant.

```
BOUCLE pour i allant de 1 à longueur de la liste - 1
    temp ← élément i de liste_nombres
    j ← i
    BOUCLE tant que j strictement supérieur à 0 et élément j - 1 de liste_nombres supérieur ou égale à temp
        élément j de liste_nombres ← élément j - 1 de liste_nombres
        j ← j - 1
    élément j de liste_nombres ← temp
```

2\. Montrer que la complexité en temps dans le pire des cas de cet algorithme est $`\mathrm{T(n) = 5.n^2 + 2.n}`$, avec $`\mathrm{n}`$ la longueur de la liste.

3\. Montrer que l'algorithme se termine.

4\. Montre que l'algorithme est correct.

5\. Exécuter l'algorithme pour différentes valeurs de n. On prendra `n = 20`, `n = 40`, `n = 60`, ..., jusque `n = 200`. Noter les valeurs obtenues pour le temps d'exécution $`\mathrm{\Delta t}`$. On travaillera dans le pire des cas, c'est à dire lorsque l'on trie une liste déjà triée dans l'autre sens.

6\. Tracer le nuage de points $`\mathrm{\Delta t = f(n)}`$. Quel type de courbe obtient-on ? Est-ce attendu ? Expliquer.
