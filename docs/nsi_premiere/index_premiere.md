# Classe de première


## Consignes de rentrée

- **le cours :** il s'articule entre cours au tableau appuyé d'un diaporama, travaux pratiques avec ou sans ordinateur et travaux dirigés sans ordinateur.

- **le travail à la maison :** il consiste à rédiger au propre le cours en markdown, faire les exercices, devoirs à la maison, avancer les projets et réviser le tout régulièrement.

- **la préparation au baccalauréat :** il faut travailler régulièrement les annales des années précédentes et tous les exercices pratiques de la banque nationale des sujets afin de bien se préparer aux épreuves écrites et pratiques.

- **les évaluations :** elles consistent en des devoirs surveillés écrits et pratiques sanctionnés par une note sur 20. Les devoirs à la maison, les oraux, les projets et le cours en markdown sont aussi évalués sous la forme d'un bonus entre 0 et 2 points. La moyenne des bonus du trimestre augmente la moyenne. Ainsi, un travail personnel et sérieux permet facilement d'augmenter sa moyenne de 1 point même s'il est incomplet ou contient des erreurs. Un travail non rendu est sanctionné par un malus de 0.5 points. Un travail bâclé ou recopié est automatiquement évalué à 0 points.

- **matériel conseillé**:

    - une clé USB : pour sauvegarder programmes et documents. Ne pas oublier de sauvegarder son contenu chez soi en cas de perte ou de dysfonctionnement.
    - un classeur ou un porte-vues : pour ranger les documents papiers distribués en cours
    - un grand cahier : pour prendre des notes afin de pourvoir rédiger son cours au propre, noter les réponses aux diverses questions des TP et TD et les remarques.


## Langages et programmation - Mise au point des programmes, gestion des bugs

Un langage de programmation est un langage formel. Son utilisation impose une rigueur mathématique pour l'écrire, l'interpréter et obtenir le bon comportement de la machine. Programmer n'est donc pas une tâche facile. Tout au long de l'apprentissage, le novice, tout au long de son travail, l'expert, rencontre erreurs et bugs. L'expérience personnelle et l'application des bonnes pratiques, issues de l'expérience des experts, permettra d'améliorer quotidiennement la qualité des programmes rédigés.

Pendant l'année, les problématiques classiques seront exposées, les situations rencontrées seront expliquées avec propositions de solutions.

- Prise en main de Python et bonnes pratiques : [md](../outils/initiation_python.md)

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/)

- Parcours Castor : [lien](https://concours.castor-informatique.fr/)

- Parcours Algorea : [lien](https://parcours.algorea.org/contents/4703/)

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/)


## Architecture de l'ordinateur : l'ordinateur de Von Neumann

L'idée d'ordinateur comme nous la connaissons ne s'est pas faite en un jour. Elle s'est construite sur une longue évolution des idées mathématiques et des techniques, d'abord mécaniques, puis scientifiques, qui remonte à plus de 40000 ans avant notre ère, époque de l'Homme de Cro-Magnon. C'est en effet notre besoin de compter, mémoriser et calculer qui nous a conduit à construire des machines nous permettant de le faire de manière plus efficace. Cette évolution a été marquée par trois accélérations remarquables. Au XIIIème siècle, l'avènement de la mécanique de précision a introduit l'automatisation des machines. Au XIXème siècle, l'industrialisation de l'électricité rend les machines électriques et au milieu du XXème siècle, Turing, Von Neumann et leurs collaborateurs définissent et construisent l'ordinateur moderne. La miniaturisation du composant principal des machines, le transistor, n'a eu de cesse de doubler les performances des ordinateurs jusqu'à ce jour.

Après la présentation de l'architecture de Von Neumann des processeurs, leur programmation en langage machine et en assembleur est abordée.

- Cours : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/Cours_NSI_Architecture.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/Cours_NSI_Architecture.odp)
- Activités pour comprendre le fonctionnement d'un processeur et l'assembleur : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/TP_assembleur.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/TP_assembleur.odt)
- Fiches processeur vierges : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/schema_neumann_vierge.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/schema_neumann_vierge.odp)
- Compléments sur les processeurs et les mémoires : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/processeurs_et_memoires.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/processeurs_et_memoires.odp)


## Python - Constructions élémentaires - Programmation structurée

Les paradigmes de programmation impérative et structurée sont présentés en Python. La programmation structurée est opposée à la programmation dite "spaghetti" largement diffusée à travers le langage BASIC, qui a permis aux amateurs éclairés des années 70 de s'initier à la programmation et aux petites entreprises d'intégrer l'outil informatique. C'est en développant et commercialisant ce langage pour l'Altaïr 8800, premier micro-ordinateur grand public, que Bill Gates, agé de 19 ans, et Paul Allen, agé de 22 ans, fondent Micro-soft en 1975.

- TP-cours sur le gestion du flux d'instructions : [pdf](./03_python_constructions_elementaires/Cours_Python_02_Gestion_flux_instructions.pdf) ou [odt](./03_python_constructions_elementaires/Cours_Python_02_Gestion_flux_instructions.odt)
- Fiche d'exercices écrits et pratiques : [pdf](./03_python_constructions_elementaires/Exercices_Python_01.pdf) ou [odt](./03_python_constructions_elementaires/Exercices_Python_01.odt)
- Compléments sur l'affectation et le typage des données : [pdf](./02_python_demarrer/Complements_affectation_typage.pdf) ou [odt](./02_python_demarrer/Complements_affectation_typage.odt)
- Compléments sur les fonctions `print` et `input` : [pdf](./02_python_demarrer/Complements_print_input.pdf) ou [odt](./02_python_demarrer/Complements_print_input.odt)
- Devoir à la maison pour les vacances de la Toussaint : [pdf](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux.pdf) ou [odt](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux.odt)


## Corrections

[Dépôt des corrections](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_premiere/corrections_eleves)
