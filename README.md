# Spécialité Numérique et Sciences Informatiques

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------

## Présentation

Supports de cours, TD et TP pour la spécialité NSI.

[Poursuivre](./docs/index.md)

--------------------
